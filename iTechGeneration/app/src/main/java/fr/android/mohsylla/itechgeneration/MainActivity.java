package fr.android.mohsylla.itechgeneration;

import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import fr.android.mohsylla.itechgeneration.Fragments.AddSoundInServerFragment;
import fr.android.mohsylla.itechgeneration.Fragments.CurrentListPlayerFragment;
import fr.android.mohsylla.itechgeneration.Fragments.FoodFragment;
import fr.android.mohsylla.itechgeneration.Fragments.GameFragment;
import fr.android.mohsylla.itechgeneration.Fragments.MenuFragment;
import fr.android.mohsylla.itechgeneration.Fragments.MusicFragment;

public class MainActivity extends AppCompatActivity {
    private static  final int PERMISSION_REQUEST_CODE = 1; // permission var
    private FragmentTransaction fragmentTransaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // SDK> 23 permission
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                // Code for above or equal 23 API Oriented Device
                // Your Permission granted already .Do next code
            } else {
                requestPermission(); // Code for permission
            }
        }
        else
        {

            // Code for Below 23 API Oriented Device
            // Do next code
        }



        // Initialize fragment
        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,new MusicFragment());
        fragmentTransaction.commit();


    /*    fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,new CurrentListPlayerFragment());
        fragmentTransaction.commit();
        fragmentTransaction.addToBackStack(null); */






        // color actionBar title and ActionBar text Intro
        try {
            getSupportActionBar().setTitle(Html.fromHtml("<font color='#FF4081'>PlayMySelf</font>"));
        }
        catch (Exception e)
        {
            Log.d("EXCEPT","Erreur"+e);
        }





        final BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);





        // set Fragments
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.music_icon:

                        getSupportActionBar().setTitle("PlayMySelf");
                        getSupportActionBar().setTitle(Html.fromHtml("<font color='#FF4081'>PlayMySelf</font>"));


                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new MusicFragment());
                        fragmentTransaction.commit();
                        fragmentTransaction.addToBackStack(null);
                        item.setChecked(true);
/*


                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new CurrentListPlayerFragment());
                        fragmentTransaction.commit();
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.hide(new CurrentListPlayerFragment());
                        item.setChecked(true);

                       fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new AddSoundInServerFragment());
                        fragmentTransaction.commit();
                        fragmentTransaction.addToBackStack(null);
                        item.setChecked(true);
 */




                        break;

                    case R.id.food_icon:

                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new FoodFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Food");
                        item.setChecked(true);

                        break;

                    case R.id.menu_icon:

                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new MenuFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Menu");
                        item.setChecked(true);

                        break;

                    case R.id.game_icon :

                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new GameFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Game");
                        item.setChecked(true);
                        break;


                    case R.id.notification_icon :

                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new NotificationFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Notification");
                        item.setChecked(true);
                        break;


                }
                return true;
            }
        });

    }




// SDk > 23 (Lollipop) permission allow data

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }


    }


}

