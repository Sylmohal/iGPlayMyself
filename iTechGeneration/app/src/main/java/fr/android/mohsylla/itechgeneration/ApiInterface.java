package fr.android.mohsylla.itechgeneration;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by kaidstrood on 6/28/17.
 */

public interface ApiInterface {

    @POST("api/users")
    Call<List<User>> getUsers();

}
