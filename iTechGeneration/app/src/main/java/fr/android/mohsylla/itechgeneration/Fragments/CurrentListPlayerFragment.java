package fr.android.mohsylla.itechgeneration.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fr.android.mohsylla.itechgeneration.PlayersMusicAdapter;
import fr.android.mohsylla.itechgeneration.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentListPlayerFragment extends Fragment {

    Button mAddMusic;
    static RecyclerView recyclerView;
    PlayersMusicAdapter adapter;


    public CurrentListPlayerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_current_list_player, container, false);
        setHasOptionsMenu(true);

        mAddMusic = (Button) v.findViewById(R.id.buttonAddMusic);
        mAddMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                //i.setType("audio/*");
                startActivityForResult(i, 12345);
            }
        });

/*
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
//        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
*/
        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        MusicFragment musicFragment = new MusicFragment();
        fragmentTransaction.replace(R.id.main_container, musicFragment);
        fragmentTransaction.commit();

        return super.onOptionsItemSelected(item);
    }
}