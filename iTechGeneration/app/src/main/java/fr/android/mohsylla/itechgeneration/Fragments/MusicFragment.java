package fr.android.mohsylla.itechgeneration.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import fr.android.mohsylla.itechgeneration.FileUtils;
import fr.android.mohsylla.itechgeneration.PLayMe;
import fr.android.mohsylla.itechgeneration.PlayersMusicAdapter;
import fr.android.mohsylla.itechgeneration.R;
import fr.android.mohsylla.itechgeneration.User;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class MusicFragment extends Fragment {

    ImageView mImageView;
    private FragmentTransaction fragmentTransaction;


    private static final String ServerUrl = "http://192.168.1.32:3000/";
    private static final String TAG = "REST";
    @SuppressLint("StaticFieldLeak")
    static RecyclerView recyclerView;
    static String originalname = null;
    LinearLayout linearLayout, linearLayout1;
    PlayersMusicAdapter adapter;
    boolean aBoolean = true;
    ArrayList<PLayMe> pLayMeArrayList;
    ArrayList<User> userArrayList;
    private Uri uri;
    private ImageView currentPlayerImageV;
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(ServerUrl);
        } catch (URISyntaxException e) {
            Log.d("EXCEPTION", "erreur" + e);
        }
    }
    private ProgressDialog mProgressDialog;



    public MusicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



/*
          fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,new MusicFragment());
        fragmentTransaction.commit();
        fragmentTransaction.addToBackStack(null); */



/*
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,new AddSoundInServerFragment());
        fragmentTransaction.commit();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.hide(new AddSoundInServerFragment());  */



        mSocket.connect();

        View v = inflater.inflate(R.layout.fragment_music, container, false);
       mImageView = (ImageView) v.findViewById(R.id.currentPlayerImageV_id);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                View mView = getLayoutInflater().inflate(R.layout.activity_pop_up,null);
                mBuilder.setView(mView);
                AlertDialog dialog = mBuilder.create();
                dialog.show();
            }
        });





       currentPlayerImageV = (ImageView) v.findViewById(R.id.currentPlayerImageV_id);

        pLayMeArrayList = new ArrayList<>();
        userArrayList = new ArrayList<>();

      // recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        adapter = new PlayersMusicAdapter(this,pLayMeArrayList,userArrayList);

        mSocket.on("all_users_list", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("data2", args[0].toString());
                        JSONArray jsonArray = (JSONArray) args[0];
                        userArrayList.clear();

                        for (int i = 0;i < jsonArray.length(); i++){
                            try {
                                JSONObject json = jsonArray.getJSONObject(i);
                                String name = json.getString("nickName");
                                String urlPath = json.getString("urlProfile");
                                String userId = json.getString("userID");

                                User user = new User();
                                user.setUserID(userId);
                                user.setNickName(name);
                                user.setUrlProfile(urlPath);
                                userArrayList.add(user);

                            }catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                       // recyclerView.invalidate();

                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        mSocket.on("playMySelf_list", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {


                        Log.v("data", args[0].toString());

                        JSONArray jsonArray = (JSONArray) args[0];
                        pLayMeArrayList.clear();

                        if(jsonArray.length() != 0){
                            linearLayout1.setVisibility(View.GONE);
                            linearLayout.setVisibility(View.VISIBLE);
                            for (int i = 0; i < jsonArray.length(); i++) {

                                try {
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    String name = json.getString("musicName");
                                    String user_id = json.getString("userID");
                                    String id = json.getString("_id");

                                    PLayMe pLayMe = new PLayMe();
                                    pLayMe.set_id(id);
                                    pLayMe.setUserID(user_id);
                                    pLayMe.setMusicName(name);
                                    pLayMeArrayList.add(pLayMe);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            //RETREIVE CURRENT PLAYER IN THREAD
                            //ON RECUPERE EN HAUT LA LIST DES USERS ET DES PLAYER
                            //ON RECUPERE LE PREMIER DANS LE TABLEAU DES PLAYER QUI EST EN
                            // FAITE LE CURRENT USER ET ON RECUPERE SES DONNEE A PARTIR DE
                            // SON ID DANS LA LIST DES USERS


                            if(pLayMeArrayList.size() != 0){
                                PLayMe player = pLayMeArrayList.get(0);
                                for (int i = 0; i < userArrayList.size();i++){
                                    User user = userArrayList.get(i);
                                    int test = player.getUserID().compareTo(user.getUserID());
                                    if(test == 0){
                                        Picasso.with(getContext()).load(user.getUrlProfile()).into(currentPlayerImageV);
                                    }

                                }
                            }

                            if (pLayMeArrayList.size() >=2){
                                pLayMeArrayList.remove(0);
                            }else {
                                pLayMeArrayList.clear();
                            }


                          //  recyclerView.invalidate();

                            adapter.notifyDataSetChanged();

                        } else {
                            // il n'y pas de music dans le server
                            linearLayout1.setVisibility(View.VISIBLE);
                            linearLayout.setVisibility(View.GONE);
                        }


                    }


                });


            }
        });



        setHasOptionsMenu(true);

/*

      recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter.notifyDataSetChanged(); */


       linearLayout = (LinearLayout) v.findViewById(R.id.action_doing);
       linearLayout1 = (LinearLayout) v.findViewById(R.id.no_action_doing);
        return v;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

     FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        CurrentListPlayerFragment currentListPlayerFragment = new CurrentListPlayerFragment();
        fragmentTransaction.replace(R.id.main_container, currentListPlayerFragment);
        fragmentTransaction.commit();





        //FragmentManager manager = ((AppCompatActivity)getContext()).getFragmentManager();
      /*  FragmentTransaction fragmentTransaction = manager.beginTransaction();
        CurrentListPlayerFragment fragmentPieces2 = new NombredePiecesFragment();
        fragmentTransaction2.replace(R.id.main_container, fragmentPieces2);
        fragmentTransaction2.addToBackStack(null);
        fragmentTransaction2.commit(); */

        // pick music
     /*   int id = item.getItemId();
        if (id == R.id.music_action) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
            //i.setType("audio/*");

            startActivityForResult(i, 12345);

            return true;

        } */



        return super.onOptionsItemSelected(item);



/*
        int id = item.getItemId();
        if(id == R.id.music_action){
            Intent i = new Intent(getActivity(), CurrentPlayerListActivity.class);
            startActivity(i);

        }

        return super.onOptionsItemSelected(item); */
   }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 12345) {

            if (resultCode == RESULT_OK) {

                try {
                    Uri uri = data.getData();
                    String path;
                    path = FileUtils.getPath(getContext(), uri);
                    originalname = FileUtils.getFileNameByUri(getContext(), uri);
                    Toast.makeText(getContext(), path, Toast.LENGTH_SHORT).show();
                    Toast.makeText(getContext(), originalname, Toast.LENGTH_SHORT).show();
                    AsyncHttpClient client = new AsyncHttpClient();
                    assert path != null;
                    File myFile = new File  (path);
                    RequestParams params = new RequestParams();


                    try {
                        params.put("myFile", myFile, "audio/mp3");
                        //params.put("myFile",myFile);
                        client.post(ServerUrl + "api/upload", params, new AsyncHttpResponseHandler() {


                            @Override
                            public void onStart() {
                                super.onStart();
                                Toast.makeText(getContext(), "started", Toast.LENGTH_SHORT).show();
                                compute();

                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                try {
                                    JSONObject json = new JSONObject();
                                    json.put("userID", "itech-00_30");
                                    json.put("musicName", originalname);
                                    mSocket.emit("add_music", json);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getContext(), "success", Toast.LENGTH_SHORT).show();
                                linearLayout1.setVisibility(View.GONE);
                                linearLayout.setVisibility(View.VISIBLE);


                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onFinish() {
//                                Toast.makeText(getContext(), "finished", Toast.LENGTH_SHORT).show();
                                super.onFinish();
                                mProgressDialog.hide();
                            }

                            @Override
                            public void onProgress(long bytesWritten, long totalSize) {
//                                Toast.makeText(getContext(), "uploading", Toast.LENGTH_SHORT).show();
                                super.onProgress(bytesWritten, totalSize);


                            }
                        });
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "File not found :" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "URI not found :" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }

        super.onActivityResult(requestCode, resultCode, data);

    }
    private void compute() {
        mProgressDialog = ProgressDialog.show(getActivity(), "Veuillez Patienter", "Ajout de la musique en cours...", true);
    }
    public void getData() {

    }




}




