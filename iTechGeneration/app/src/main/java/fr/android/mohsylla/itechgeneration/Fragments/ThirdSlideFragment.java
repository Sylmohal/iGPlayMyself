package fr.android.mohsylla.itechgeneration.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import fr.android.mohsylla.itechgeneration.BeforeMainActivity;
import fr.android.mohsylla.itechgeneration.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Mohsylla on 19/09/2017.
 */

public class ThirdSlideFragment extends Fragment {

    ImageView mRegisterImageOne;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    Button mButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.third_slide,container,false);


        mButton = (Button) v.findViewById(R.id.button3);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),BeforeMainActivity.class);
                startActivity(intent);

            }

        });

        mRegisterImageOne = (ImageView) v.findViewById(R.id.imageTest);

        mRegisterImageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });


       return v;
    }



    private void openGallery (){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);

    }

    @Override
    public void onActivityResult( int requestCode , int resultCode , Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {

            imageUri = data.getData();
            mRegisterImageOne.setImageURI(imageUri);


        }
    }
}
