package fr.android.mohsylla.itechgeneration;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.android.mohsylla.itechgeneration.Fragments.MusicFragment;

/**
 * Created by Mohsylla on 09/06/2017.
 */

public class PlayersMusicAdapter extends RecyclerView.Adapter<PlayersMusicAdapter.MyViewHolder> {


    MusicFragment context;
    ArrayList<PLayMe> data;
    ArrayList<User> userList;
    LayoutInflater inflater;

    public PlayersMusicAdapter(MusicFragment context, ArrayList<PLayMe> data,ArrayList<User> userData) {
        this.context = context;
        this.data = data;
        this.userList = userData;
        inflater = LayoutInflater.from(context.getActivity());
        Log.v("data", String.valueOf(data.size()));

    }


    @Override
    public PlayersMusicAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.list_item_musicplayers, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlayersMusicAdapter.MyViewHolder myViewHolder, int position) {

       /* PLayMe dataMusic = data.get(position);
        myViewHolder.setUserPlayer(dataMusic.getUserID()); */
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView userNameTextView;
      //  TextView textView1;
        final Context context;
        private ImageView userImage;


        public MyViewHolder(View itemView) {
            super(itemView);
            //   textView = (TextView) itemView.findViewById(R.id.txtlol);
            userNameTextView = (TextView) itemView.findViewById(R.id.username_id);
            userImage = (ImageView) itemView.findViewById(R.id.userimage_id);
            context = itemView.getContext();
        }
        public void setUserPlayer(String id){
            for (int i = 0; i < userList.size();i++){
                User user = userList.get(i);
                Log.i("Don", user.getUserID()+"->"+id);

                int test = id.compareTo(user.getUserID());
                if(test == 0){
                    userNameTextView.setText(user.getNickName());
                    Picasso.with(context).load(user.getUrlProfile()).into(userImage);
                }
            }
        }

    }
}
