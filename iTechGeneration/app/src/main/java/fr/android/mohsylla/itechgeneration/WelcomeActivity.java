package fr.android.mohsylla.itechgeneration;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import fr.android.mohsylla.itechgeneration.Fragments.ThirdSlideFragment;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager mPager;
    private int[] layouts = {R.layout.first_slide, R.layout.second_slide , R.layout.third_slide,R.layout.fourth_slide ,R.layout.fifth_slide };
private onBoardingAdapter adapter;
    Uri imageUri;

    Button mButton;

    ImageView mRegisterImageOne;
    private static final int PICK_IMAGE = 100;

private LinearLayout Dots_Layout;
private ImageView[] dots;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);

/*
        ThirdSlideFragment thirdSlideFragment = new ThirdSlideFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.okeuh,thirdSlideFragment);
        transaction.commit();
       */




     //   LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater inflater = getLayoutInflater();


        View v = inflater.inflate(R.layout.third_slide, null);
      //  RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.testRela);





        mButton = (Button) v.findViewById(R.id.button3);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),BeforeMainActivity.class);
                startActivity(intent);

            }

        });



//        relativeLayout.addView(v);

/*
        mRegisterImageOne = (ImageView) third_slide_view.findViewById(R.id.imageTest);

        mRegisterImageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        }); */

/*
        if (Build.VERSION.SDK_INT>=19)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
*/



        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mPager = (ViewPager) findViewById(R.id.viewPager);
        adapter = new onBoardingAdapter(layouts,this);
        mPager.setAdapter(adapter);
        Dots_Layout = (LinearLayout) findViewById(R.id.dotsLayout);
        createDots(0);






        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }






    private void createDots(int current_position){
        if(Dots_Layout != null){
            Dots_Layout.removeAllViews();

            dots = new ImageView[layouts.length];
        }

        for(int i = 0 ; i< layouts.length ; i++)
        {
            dots[i] = new ImageView(this);
            if (i == current_position){

                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
            } else {

                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.inactive_dots));
            }

LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4,0,4,0);

Dots_Layout.addView(dots[i],params);
        }

    }





    private void openGallery (){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);

    }

    @Override
    protected void onActivityResult( int requestCode , int resultCode , Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {

            imageUri = data.getData();
            mRegisterImageOne.setImageURI(imageUri);


        }
    }






}
