package fr.android.mohsylla.itechgeneration;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kaidstrood on 6/28/17.
 */

public class User {

    @SerializedName("_id")
    private String _id;

    @SerializedName("urlProfile")
    private String urlProfile;

    @SerializedName("nickName")
    private String nickName;

    @SerializedName("userID")
    private String userID;

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setUrlProfile(String urlProfile) {
        this.urlProfile = urlProfile;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String get_id() {
        return _id;
    }

    public String getUrlProfile() {
        return urlProfile;
    }

    public String getNickName() {
        return nickName;
    }

    public String getUserID() {
        return userID;
    }
}

