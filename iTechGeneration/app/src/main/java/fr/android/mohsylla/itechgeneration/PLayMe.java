package fr.android.mohsylla.itechgeneration;

/**
 * Created by Mohsylla on 16/06/2017.
 */

public class PLayMe {

    private String userID ;
    private String musicName;
    private String _id ;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
