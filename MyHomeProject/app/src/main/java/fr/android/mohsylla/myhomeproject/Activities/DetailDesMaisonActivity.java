package fr.android.mohsylla.myhomeproject.Activities;

import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import fr.android.mohsylla.myhomeproject.Adapter.TabPagerParallaxAdapter;
import fr.android.mohsylla.myhomeproject.Fragment.GalleryFragment;
import fr.android.mohsylla.myhomeproject.Fragment.InfoParallaxFragment;
import fr.android.mohsylla.myhomeproject.Fragment.PaymentFragment;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

public class DetailDesMaisonActivity extends AppCompatActivity {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ImageView imgHeader;

    FirebaseStorage storage = FirebaseStorage.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_recycler_details);
        SetUpHeaderAndNavigation();

        View view = findViewById(R.id.detailsHouse);

     /* Snackbar snackbar = Snackbar.make(view,"SnackBar",Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(R.color.colorMyhome);
        snackbar.show(); */
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void SetUpHeaderAndNavigation() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

      //  toolbar.setNavigationIcon(R.mipmap.deleteview_icon);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       getSupportActionBar().setDisplayShowTitleEnabled(false);


        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        imgHeader = (ImageView) findViewById(R.id.img_header);


        HomeModel homeModel = getIntent().getParcelableExtra("homeData");
        SetImage(homeModel);

        TabPagerParallaxAdapter mAdapter = new TabPagerParallaxAdapter(getSupportFragmentManager());

        InfoParallaxFragment infoParallaxFragment = new InfoParallaxFragment();
        infoParallaxFragment.setHomedata(homeModel);
        mAdapter.addFragment(infoParallaxFragment, "Infos");

      /*  PaymentFragment paymentFragment = new PaymentFragment();
        paymentFragment.setHomeData(homeModel);
        mAdapter.addFragment(paymentFragment , "Payment"); */

        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.setKey_of_home(homeModel.getKey());
        mAdapter.addFragment(galleryFragment, "Gallerie");

            mViewPager.setAdapter(mAdapter);
            mTabLayout.setupWithViewPager(mViewPager);

    }
    public void SetImage(HomeModel item){
        StorageReference storageReference = storage.getReference(item.getPath());
        Glide.with(getApplicationContext().getApplicationContext())
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .into(imgHeader);
    }
}
