package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

// import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.LocationAdapter;
import fr.android.mohsylla.myhomeproject.Datas.AmbergurView;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

public class LocalityActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

TextView tv_header_location;

LocationAdapter adapter;



    private String budgetChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<String> LocalityList;
    private String NombreDePieceChoisi;
    private ArrayList<HomeModel> DataFinaly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

      /*  tv_header_location = (TextView) findViewById(R.id.tv_item_header_location);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Medium.ttf");
        tv_header_location.setTypeface(myCustomFont); */




        // / drawer layout



        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        budgetChoisi = getIntent().getStringExtra("budgetChoisi");
        HomeData = getIntent().getParcelableArrayListExtra("homeData");
        NombreDePieceChoisi = getIntent().getStringExtra("nombreDePiece");

        DataFinaly = new ArrayList<>();
        for (HomeModel item : HomeData ) {
            int result = NombreDePieceChoisi.compareTo(String.valueOf(item.getRooms()));
            if(result == 0){
                int resultSelonBudget = budgetChoisi.compareTo(item.getPrice());
                if(resultSelonBudget == 0){
                    Log.v("local",item.getPrice());
                    DataFinaly.add(item);
                }

            }
        }





        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        adapter = new LocationAdapter(this , DataFinaly);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        MenuItem item = menu.findItem(R.id.action_search);
       SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }








}
