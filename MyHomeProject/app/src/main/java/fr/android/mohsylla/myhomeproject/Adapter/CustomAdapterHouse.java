package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.DetailDesMaisonActivity;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 03/05/2017.
 */



public class CustomAdapterHouse extends RecyclerView.Adapter<CustomAdapterHouse.MyViewHolder> {

    Context context;
    ArrayList<HomeModel> data;
    LayoutInflater inflater;

    public CustomAdapterHouse(Context context ,  ArrayList<HomeModel> data) {

        this.context= context;
        this.data = data;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.list_item_house_recycler,parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

     myViewHolder.SetImage(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private final Context context;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        private TextView nomDePieceTextView;
        private TextView priceTextView;
        private TextView localiteTextView;
        View mView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.img_row);
            nomDePieceTextView = (TextView)itemView.findViewById(R.id.id_nombreDePiece);
            priceTextView = (TextView)itemView.findViewById(R.id.id_price);
            localiteTextView = (TextView)itemView.findViewById(R.id.id_localite);

            context = itemView.getContext();
            imageView.setOnClickListener(this);
        }
        public void SetImage(HomeModel item){
            StorageReference storageReference = storage.getReference(item.getPath());
            Glide.with(context.getApplicationContext())
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(imageView);
            priceTextView.setText(item.getPrice()+" FCFA");
            nomDePieceTextView.setText(String.valueOf(item.getRooms())+" Pieces");
            localiteTextView.setText(item.getStreet());
        }


        @Override
        public void onClick(View view) {
            final Intent intent;
            int position = getAdapterPosition();
            intent =  new Intent(context.getApplicationContext(), DetailDesMaisonActivity.class);
            intent.putExtra("homeData",data.get(position));
            context.startActivity(intent);
        }
    }
}
