package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import fr.android.mohsylla.myhomeproject.Adapter.AppartAdapter;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NombredePiecesFragment extends Fragment {

    TextView mNOMBREPIECES , mCHOISISSEZPIECES;
    AppartAdapter adapter;
    private int selectedRoom;
    private String typeDeBienChoisi;
    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");
    //  private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //  private NavigationView navigationView;
    private ArrayList<HomeModel> HomeData ;
    private ArrayList<Integer> NomDePieceList;

    public NombredePiecesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_nombre_de_pieces, container, false);
        setRetainInstance(true);


        mNOMBREPIECES = (TextView) view.findViewById(R.id.titlePIECES);
     /*   Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Heavy.ttf");
        mNOMBREPIECES.setTypeface(myCustomFont); */

        mCHOISISSEZPIECES = (TextView) view.findViewById(R.id.choisirNOMBREPIECES);
       /* Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Book.ttf");
        mCHOISISSEZPIECES.setTypeface(myCustomFont1); */

        HomeData = new ArrayList<HomeModel>();
        NomDePieceList = new ArrayList<>();


        Bundle bundle = getArguments();
        typeDeBienChoisi =  bundle.getString("typeDeBien");






     //   typeDeBienChoisi = getActivity().getIntent().getStringExtra("typeDeBien");
        //Log.v("type",typeDeBienChoisi);










        // drawer layout

       // getSupportActionBar().setTitle("");
        //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //   getSupportActionBar().setHomeButtonEnabled(true);
//        drawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout1);
        //   actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.drawer_open,R.string.drawer_close);
        //    drawerLayout.setDrawerListener(actionBarDrawerToggle);


        //  AmbergurView ambergurView = new AmbergurView(this);
        //  ambergurView.SetUpNavigation();



        final RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);



        listingsReference
                .orderByChild("type")
                .equalTo(typeDeBienChoisi)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for(DataSnapshot child : dataSnapshot.getChildren()){
                                    HomeModel home = child.getValue(HomeModel.class);
                                    home.setKey(child.getRef().getKey().toString());
                                    HomeData.add(home);
                                    NomDePieceList.add(home.getRooms());
                                }
                                /*
                        * TRIE DANS L'ORDRE DECROISSANT DU NOMBRE DE PIECE
                         */
                                Collections.sort(NomDePieceList);
                        /*
                        * RECUPERATION DU NOMBRE DE PIECE IRREPETITIF
                         */
                                Boolean conditionDelete = true;
                                while (conditionDelete){
                                    conditionDelete = false;
                                    for(int i = 0;i < NomDePieceList.size();i++){
                                        if(i < NomDePieceList.size()-1){
                                            if(NomDePieceList.get(i) == NomDePieceList.get(i+1)){
                                                NomDePieceList.remove(i);
                                                conditionDelete = true;
                                            }
                                        }

                                    }
                                }
                                mRecyclerView.invalidate();
                                adapter.notifyDataSetChanged();
                            }
                        });


                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.v("ERROR",databaseError.toString());
                    }
                });

        adapter = new AppartAdapter(getActivity(),NomDePieceList,HomeData);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        //set the adapter
        mRecyclerView.setAdapter(adapter);
        return view;
    }



}
