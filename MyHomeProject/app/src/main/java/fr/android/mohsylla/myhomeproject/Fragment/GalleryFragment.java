package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.InfoGalleryAdapter;
import fr.android.mohsylla.myhomeproject.Model.GalleryModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment {

    RecyclerView recyclerView;
    InfoGalleryAdapter adapter;
    private String key_of_home;
    private ArrayList<String> GalleryList;
    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("galls");

    public String getKey_of_home() {
        return key_of_home;
    }

    public void setKey_of_home(String key_of_home) {
        this.key_of_home = key_of_home;
    }

    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gallery,container,false);

        GalleryList = new ArrayList<>();

        listingsReference
                .orderByChild("propid")
                .equalTo(key_of_home)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    GalleryModel galleryModel = snapshot.getValue(GalleryModel.class);
                                    GalleryList.add(galleryModel.getPath());
                                }
                                adapter.notifyDataSetChanged();
                                recyclerView.invalidate();

                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview1);
        adapter = new InfoGalleryAdapter(this ,GalleryList);


        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3, GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);

        return v;


    }


}
