package fr.android.mohsylla.myhomeproject.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.InfoParallaxAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataInfoFragment;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Info;
import fr.android.mohsylla.myhomeproject.R;


public class InfoParallaxFragment extends Fragment {
    private View rootView;
    private RecyclerView mRecyclerView;
    InfoParallaxAdapter adapter;
    public HomeModel Homedata;

    public HomeModel getHomedata() {
        return Homedata;
    }

    public void setHomedata(HomeModel homedata) {
        Homedata = homedata;
    }

    private ArrayList<Info> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_info_parallax_fragment, null);
            initialize();
        }
        TextView DescriptionTextView = (TextView)rootView.findViewById(R.id.id_description);
        DescriptionTextView.setText(Homedata.getDescription());

        return rootView;
    }

    private void initialize() {

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);


        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));

        adapter = new InfoParallaxAdapter(getActivity(), DataInfoFragment.getData(),Homedata);

        mRecyclerView.setAdapter(adapter);
    }


}
