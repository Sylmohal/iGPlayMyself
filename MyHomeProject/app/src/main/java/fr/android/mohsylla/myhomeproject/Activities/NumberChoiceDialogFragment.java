package fr.android.mohsylla.myhomeproject.Activities;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 17/08/2017.
 */

public class NumberChoiceDialogFragment extends DialogFragment {

    Button minusBtn , plusBtn;
    int minteger = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.activity_alert, container, false);


        minusBtn = (Button) view.findViewById(R.id.decrease);
        plusBtn = (Button) view.findViewById(R.id.increase);

        return view;
    }

    public void increaseInteger(View view) {
        minteger = minteger + 1;
        display(minteger);

    }public void decreaseInteger(View view) {
        minteger = minteger - 1;
        display(minteger);
    }

    private void display(int number) {
        TextView displayInteger = (TextView) getView().findViewById(R.id.integer_number);
        if(number<0 || number == 0) {

            minusBtn.setVisibility(View.GONE);

        }
        else {
            minusBtn.setVisibility(View.VISIBLE);
        }

        displayInteger.setText("" + number);
    }


    public void show(FragmentManager fragmentManager, String oook) {


    }
}
