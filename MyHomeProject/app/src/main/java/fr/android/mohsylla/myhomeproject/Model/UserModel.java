package fr.android.mohsylla.myhomeproject.Model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by bios on 22/07/2017.
 */
@IgnoreExtraProperties
public class UserModel {
    public String username;
    public String email;
    public String ID;

    public UserModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public UserModel(String username, String email,String ID) {
        this.username = username;
        this.email = email;
        this.ID = ID;
    }

}
