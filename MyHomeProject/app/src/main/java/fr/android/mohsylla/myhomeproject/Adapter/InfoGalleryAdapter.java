package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.GalleryFragmentDetails;
import fr.android.mohsylla.myhomeproject.Fragment.GalleryFragment;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.InfoGallery;

import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 09/06/2017.
 */

public class InfoGalleryAdapter extends RecyclerView.Adapter<InfoGalleryAdapter.MyViewHolder> {

    GalleryFragment context;
    ArrayList<String> data;
    LayoutInflater inflater;


    public InfoGalleryAdapter (GalleryFragment context ,ArrayList<String> list){

        this.context= context;
        this.data = list;
        inflater = LayoutInflater.from(context.getActivity());

    }


    @Override
    public InfoGalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.from(parent.getContext()).inflate(R.layout.list_item_info_gallery,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(InfoGalleryAdapter.MyViewHolder holder, int position) {
        holder.SetImage(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        ArrayList<InfoGallery> data = new ArrayList<>();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final  Context context;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_row);
            this.context = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        public void SetImage(String path){
            StorageReference storageReference = storage.getReference(path);
            Glide.with(context.getApplicationContext())
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(imageView);
        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
//            InfoGallery infoGallery = this.data.get(position);
//            Intent intent = new Intent(this.context, GalleryFragmentDetails.class);
//            intent.putExtra("imageId", infoGallery.getImageId());
//            this.context.startActivity(intent);

        }
    }
}
