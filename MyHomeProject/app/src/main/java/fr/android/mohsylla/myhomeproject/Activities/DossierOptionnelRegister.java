package fr.android.mohsylla.myhomeproject.Activities;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.R;

public class DossierOptionnelRegister extends AppCompatActivity {

    Button mValider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dossier_optionnel_register);
      //  getSupportActionBar().hide();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOptionnelActivity);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);



        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mValider = (Button) findViewById(R.id.buttonValider);
        mValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getApplicationContext(), TypeDeBiensActivity.class);
                startActivity(intent);*/
                FragmentManager fragmentManager3 = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager3.beginTransaction();
                MeFragment meFragment = new MeFragment();
                fragmentTransaction.replace(R.id.main_container, meFragment);
                fragmentTransaction.commit();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }
}
