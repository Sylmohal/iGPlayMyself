package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Info;
import fr.android.mohsylla.myhomeproject.Model.InformationHouse;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 12/05/2017.
 */

public class InfoParallaxAdapter extends RecyclerView.Adapter<InfoParallaxAdapter.InfoParallaxViewHolder> {

    private Context mContext;
    private ArrayList<Info> data = new ArrayList<>();
    private HomeModel homeData;

    public InfoParallaxAdapter(Context context, ArrayList<Info> data, HomeModel home){
        mContext = context;
        this.data = data;
        this.homeData = home;
    }

    @Override
    public InfoParallaxAdapter.InfoParallaxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InfoParallaxViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_info_parallax, parent, false));
    }


    @Override
    public void onBindViewHolder(InfoParallaxAdapter.InfoParallaxViewHolder holder, int position) {
        Info info = data.get(position);
        holder.imgIcon.setImageResource(data.get(position).getImageId());
        holder.tvTitle.setText(info.getTitle());
        switch (position){
            case 0: holder.tvCount.setText(""+ String.valueOf(homeData.getBedrooms())); break;
            case 1: holder.tvCount.setText(""+ String.valueOf(homeData.getKitchens())); break;
            case 2: holder.tvCount.setText(""+ String.valueOf(homeData.getBathrooms())); break;
            case 3: holder.tvCount.setText(""+ String.valueOf(homeData.getLivingrooms())); break;
            //case 4: holder.tvCount.setText(""+ String.valueOf(homeData.getGarages()));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class InfoParallaxViewHolder extends RecyclerView.ViewHolder {

        ImageView imgIcon;
        TextView tvTitle, tvCount;

        public InfoParallaxViewHolder(View itemView) {
            super(itemView);

            imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
        }
    }
}
