package fr.android.mohsylla.myhomeproject.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fr.android.mohsylla.myhomeproject.Activities.TypeDeBiensActivity;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OptionelRegisterFragment extends Fragment {

    Button mValider;

    public OptionelRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_optionel_register, container, false);
     //   ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        mValider = (Button) v.findViewById(R.id.buttonValider);
        mValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                LoginFragment loginFragment = new LoginFragment();
                fragmentTransaction.replace(R.id.main_container, loginFragment);
                fragmentTransaction.commit();
            }
        });

        return v;
    }

}
