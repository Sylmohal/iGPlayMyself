package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fr.android.mohsylla.myhomeproject.R;

public class LoginActivity extends AppCompatActivity {

    Button mSeConnnecter , mEnregister;
    public FirebaseAuth mAuth;
    private EditText mEmailEditText,mPasswordEditText;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_before_main);

       // getSupportActionBar().setTitle("");
        getSupportActionBar().hide();

        preferences = getSharedPreferences("ID", Context.MODE_PRIVATE);

        mSeConnnecter = (Button) findViewById(R.id.buttonSeConnecter);
        mEnregister = (Button) findViewById(R.id.buttonEnregistrer);
        mEmailEditText = (EditText) findViewById(R.id.id_email);
        mPasswordEditText = (EditText) findViewById(R.id.id_password);

        mAuth = FirebaseAuth.getInstance();

        mSeConnnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TypeDeBiensActivity.class);
                //startActivity(intent);
                if(mEmailEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Email Vide",Toast.LENGTH_SHORT).show();
                }
                if(mPasswordEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Password Vide",Toast.LENGTH_SHORT).show();
                }

                if(mEmailEditText.getText().length() != 0 && mPasswordEditText.getText().length() != 0){
                    mAuth.signInWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){

                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("userID",task.getResult().getUser().getUid().toString());
                                        editor.commit();
                                        Intent intent = new Intent(getApplicationContext(), TypeDeBiensActivity.class);
                                        startActivity(intent);

                                    }else {
                                        Toast.makeText(getApplicationContext(),task.getException().toString(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }


            }
        });
        mEnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        String userID = preferences.getString("userID",null);
        if(userID != null){
            Intent intent = new Intent(this,TypeDeBiensActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);

        // super.onBackPressed();
        Log.v("Back","true");
    }
}
