package fr.android.mohsylla.myhomeproject.Fragment;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.zip.Inflater;

import fr.android.mohsylla.myhomeproject.Activities.RechercheMultiCriteresActivity;
import fr.android.mohsylla.myhomeproject.Adapter.TabPagerParallaxAdapter;
import fr.android.mohsylla.myhomeproject.Adapter.TypeDeBienAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataTypeDeBien;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
  /*  private TabLayout mTabLayout;
    private ViewPager mViewPager; */

    TypeDeBienAdapter adapter;
    TextView mTypeDeBiens, mChoisirTypedeBiens;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");
    private ArrayList<HomeModel> HomeData;


    public SearchFragment() {
        // Required empty public constructor
    }



   // @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_search, container, false);

     //   ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


/*
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.setStatusBarColor(getActivity().getColor(R.color.white));
        }
*/


/*
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout_searchFragment);
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager_search_fragment);


        TabPagerParallaxAdapter mAdapter = new TabPagerParallaxAdapter(getFragmentManager());



        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager); */


     //   ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        mTypeDeBiens = (TextView) view.findViewById(R.id.titleTYPEDEBIENS);


        mChoisirTypedeBiens = (TextView) view.findViewById(R.id.titleCHOISISSEZTYPEDEBIENS);


        final RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        adapter = new TypeDeBienAdapter(getActivity(), DataTypeDeBien.getData());


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        //set the adapter
        mRecyclerView.setAdapter(adapter);

      //  setHasOptionsMenu(true);

        return view;


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);



    }


    /*

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_search, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        if (id == R.id.filter_search_type) {

       Intent intent = new Intent(getActivity(), RechercheMultiCriteresActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }
*/


}