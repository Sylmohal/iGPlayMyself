package fr.android.mohsylla.myhomeproject.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Mohsylla on 11/07/2017.
 */
@IgnoreExtraProperties
public class HomeModel implements Parcelable {

    private int bathrooms;
    private int bedrooms;
    private String city;
    private String description;
    private int garages;
    private String image;
    private int kitchens;
    private int livingrooms;
    private String locality;
    private String path;
    private String price;
    private int rooms;
    private String street;
    private String title;
    private int toilets;
    private String type;
    private String key;
    private String caution;
    private String avance;

    public String getCaution() {
        return caution;
    }

    public void setCaution(String caution) {
        this.caution = caution;
    }

    public String getAvance() {
        return avance;
    }

    public void setAvance(String avance) {
        this.avance = avance;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGarages() {
        return garages;
    }

    public void setGarages(int garages) {
        this.garages = garages;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getKitchens() {
        return kitchens;
    }

    public void setKitchens(int kitchens) {
        this.kitchens = kitchens;
    }

    public int getLivingrooms() {
        return livingrooms;
    }

    public void setLivingrooms(int livingrooms) {
        this.livingrooms = livingrooms;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getToilets() {
        return toilets;
    }

    public void setToilets(int toilets) {
        this.toilets = toilets;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public HomeModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.bathrooms);
        dest.writeInt(this.bedrooms);
        dest.writeString(this.city);
        dest.writeString(this.description);
        dest.writeInt(this.garages);
        dest.writeString(this.image);
        dest.writeInt(this.kitchens);
        dest.writeInt(this.livingrooms);
        dest.writeString(this.locality);
        dest.writeString(this.path);
        dest.writeString(this.price);
        dest.writeInt(this.rooms);
        dest.writeString(this.street);
        dest.writeString(this.title);
        dest.writeInt(this.toilets);
        dest.writeString(this.type);
        dest.writeString(this.key);
        dest.writeString(this.caution);
        dest.writeString(this.avance);
    }

    protected HomeModel(Parcel in) {
        this.bathrooms = in.readInt();
        this.bedrooms = in.readInt();
        this.city = in.readString();
        this.description = in.readString();
        this.garages = in.readInt();
        this.image = in.readString();
        this.kitchens = in.readInt();
        this.livingrooms = in.readInt();
        this.locality = in.readString();
        this.path = in.readString();
        this.price = in.readString();
        this.rooms = in.readInt();
        this.street = in.readString();
        this.title = in.readString();
        this.toilets = in.readInt();
        this.type = in.readString();
        this.key = in.readString();
        this.caution = in.readString();
        this.avance = in.readString();
    }

    public static final Creator<HomeModel> CREATOR = new Creator<HomeModel>() {
        @Override
        public HomeModel createFromParcel(Parcel source) {
            return new HomeModel(source);
        }

        @Override
        public HomeModel[] newArray(int size) {
            return new HomeModel[size];
        }
    };
}
