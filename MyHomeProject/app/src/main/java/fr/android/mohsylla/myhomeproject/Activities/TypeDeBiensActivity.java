package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.TypeDeBienAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataTypeDeBien;
import fr.android.mohsylla.myhomeproject.Fragment.FavorisFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.Fragment.SearchFragment;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

public class TypeDeBiensActivity extends AppCompatActivity {

    TypeDeBienAdapter adapter;
    TextView mTypeDeBiens , mChoisirTypedeBiens;


    // private DrawerLayout drawerLayout;
    // private ActionBarDrawerToggle actionBarDrawerToggle;
    // private NavigationView navigationView;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");
    private ArrayList<HomeModel> HomeData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_de_biens);


        mTypeDeBiens = (TextView) findViewById(R.id.titleTYPEDEBIENS);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Heavy.ttf");
        mTypeDeBiens.setTypeface(myCustomFont);

        mChoisirTypedeBiens = (TextView) findViewById(R.id.titleCHOISISSEZTYPEDEBIENS);
        Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Book.ttf");
        mChoisirTypedeBiens.setTypeface(myCustomFont1);







// drawer layout


       // getSupportActionBar().setTitle("");
      /*  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setHomeButtonEnabled(true); */

      //  drawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
       // actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.drawer_open,R.string.drawer_close);
     //   drawerLayout.setDrawerListener(actionBarDrawerToggle);


        /*
        *
        *  Navaigation view drawer
         */
      //  AmbergurView ambergurView = new AmbergurView(this);
       // ambergurView.SetUpNavigation();


         final RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);

         adapter = new TypeDeBienAdapter(this, DataTypeDeBien.getData());


         mRecyclerView.setHasFixedSize(true);
         mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
         //set the adapter
         mRecyclerView.setAdapter(adapter);

    }




    @Override
    public void onBackPressed() {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);

       // super.onBackPressed();
        Log.v("Back","true");
    }

    // Nav Drawer toggle
/*
    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
