package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.CustomAdapterHouse;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AffichageDesMaisonsFragment extends Fragment {

    RecyclerView recyclerView;
    CustomAdapterHouse adapterHouse;



    private String locatliteChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<HomeModel> DataFinaly;

    public AffichageDesMaisonsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_affichage_des_maisons, container, false);

        Bundle bundle = getArguments();


        locatliteChoisi = bundle.getString("localiteChoisi");
        HomeData = bundle.getParcelableArrayList("homeData");

        DataFinaly = new ArrayList<>();
        for (HomeModel item : HomeData) {
            int result = locatliteChoisi.compareTo(item.getStreet());
            if(result == 0){
                DataFinaly.add(item);
            }
        }

        //


        //

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        adapterHouse = new CustomAdapterHouse(getActivity() , DataFinaly);

        recyclerView.setAdapter(adapterHouse);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();


        locatliteChoisi = bundle.getString("localiteChoisi");
        HomeData = bundle.getParcelableArrayList("homeData");


    }
}
