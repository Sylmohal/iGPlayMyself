package fr.android.mohsylla.myhomeproject.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import fr.android.mohsylla.myhomeproject.Fragment.FavorisFragment;
import fr.android.mohsylla.myhomeproject.Fragment.LoginFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MainFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.Fragment.SearchFragment;
import fr.android.mohsylla.myhomeproject.R;
import fr.android.mohsylla.myhomeproject.utils.BottomNavigationViewHelper;

import static fr.android.mohsylla.myhomeproject.R.id.bottomNavigationView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    Toolbar toolbar;

    private FragmentTransaction fragmentTransaction;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


      /*  toolbar = (Toolbar) findViewById(R.id.toolbarMyHome);
        setSupportActionBar(toolbar); */


      //  getSupportActionBar().setTitle("");
      //  getSupportActionBar().hide();
/*
        fragmentManager = getSupportFragmentManager();
        fragment = new MainFragment();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.main_container, fragment).commit(); */

        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_container,new MainFragment());
      //  getSupportActionBar().hide();


        //fragmentTransaction.show(new MainFragment());

    //    getFragmentManager().executePendingTransactions();
        fragmentTransaction.commit();





        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);


       // BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.id_recherche:
                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new MainFragment());
                        fragmentTransaction.commit();
                        // getSupportActionBar().setTitle("");
                        item.setChecked(true);



                        break;

                    case R.id.id_wishlist:

                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new FavorisFragment());
                        fragmentTransaction.commit();
                       // getSupportActionBar().setTitle("");
                        item.setChecked(true);


                        break;

                    case R.id.id_me:

                        fragmentTransaction  = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container,new MeFragment());
                        fragmentTransaction.commit();
                      //  getSupportActionBar().setTitle("");
                        item.setChecked(true);
                        break;
                }

                return true;

            }
        });


    }

  /*  @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    } */

}
