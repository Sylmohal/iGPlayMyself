package fr.android.mohsylla.myhomeproject.Datas;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Model.Info;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 11/07/2017.
 */

public class DataInfoFragment {

    public static ArrayList<Info> getData() {

        ArrayList<Info> data = new ArrayList<>();

        int[] images = {


                R.mipmap.chambre,
                R.mipmap.cuisine,
                R.mipmap.douche,
                R.mipmap.salon,
              //  R.mipmap.garage,


        };

        String[] title = { "CHAMBRE" , "CUISINE" , "DOUCHE" , "SALON" };

        int[] numberElements = {2 , 3 , 1 , 5 };

        for (int i = 0; i < images.length; i++) {
            Info current = new Info();
            current.setImageId(images[i]);
            current.setTitle(title[i]);
            current.setCount(numberElements[i]);
            data.add(current);

        }


        return data;
    }

}
