package fr.android.mohsylla.myhomeproject.Datas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fr.android.mohsylla.myhomeproject.Activities.AboutActivity;
import fr.android.mohsylla.myhomeproject.Activities.LoginActivity;
import fr.android.mohsylla.myhomeproject.Activities.TypeDeBiensActivity;
import fr.android.mohsylla.myhomeproject.Activities.WishlistActivity;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by bios on 21/07/2017.
 */

public class AmbergurView {
    private AppCompatActivity context;
    private NavigationView navigationView;
    private FirebaseAuth mAuth;

    public AmbergurView(AppCompatActivity context) {
        this.context = context;
    }
    public void SetUpNavigation(){
        //

        navigationView = (NavigationView) context.findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){

                    case R.id.id_home:
                        Intent intentHome = new Intent(context.getApplicationContext(), TypeDeBiensActivity.class);
                        context.startActivity(intentHome);
                        break;

                    case R.id.id_wishlist :
                        final Intent intentWish = new Intent(context.getApplicationContext(),WishlistActivity.class);
                        context.startActivity(intentWish);
                        break;

                    case R.id.id_about:
                        Intent intentAbout = new Intent(context.getApplicationContext(),AboutActivity.class);
                        context.startActivity(intentAbout);
                        break;

                    case R.id.id_deconnexion:
                        Intent intentDeconnexion = new Intent(context.getApplicationContext(),LoginActivity.class);

                        SharedPreferences preferences = context.getSharedPreferences("ID", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        context.startActivity(intentDeconnexion);

                        break;

                }
                return false;
            }
        });

    }
}
