package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import fr.android.mohsylla.myhomeproject.Model.UserModel;
import fr.android.mohsylla.myhomeproject.R;

public class RegisterActivity extends AppCompatActivity {



    Button mRegister;
    public FirebaseAuth mAuth;
    private EditText mEmailEditText, mPasswordEditText, mNameEditText;
    SharedPreferences preferences;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

//getSupportActionBar().hide();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarRegisterActivity);
        toolbar.setNavigationIcon(R.mipmap.deleteview_icon);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       getSupportActionBar().setDisplayShowTitleEnabled(false);

        //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        mNameEditText = (EditText) findViewById(R.id.id_name_register);
        mEmailEditText = (EditText) findViewById(R.id.id_email_register);
        mPasswordEditText = (EditText) findViewById(R.id.id_password_register);

        preferences = getSharedPreferences("ID", Context.MODE_PRIVATE);
        mAuth = FirebaseAuth.getInstance();

        mRegister = (Button) findViewById(R.id.buttonRegister);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DossierOptionnelRegister.class);
                startActivity(intent);

            }
        });
    }


    //startActivity(intent);
          /*     if(mEmailEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Email Vide",Toast.LENGTH_SHORT).show();
                }
                if(mPasswordEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Password Vide",Toast.LENGTH_SHORT).show();
                }
                if(mNameEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Password Vide",Toast.LENGTH_SHORT).show();
                }

                if(mEmailEditText.getText().length() != 0 && mPasswordEditText.getText().length() != 0){
                    mAuth.createUserWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){

                                        String userID = task.getResult().getUser().getUid().toString();

                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("userID",userID);
                                        editor.commit();
                                        UserModel userModel = new UserModel(mNameEditText.getText().toString(),mEmailEditText.getText().toString(),userID);
                                        myRef.child(userID).setValue(userModel);
                                        Intent intent = new Intent(getApplicationContext(), TypeDeBiensActivity.class);
                                        startActivity(intent);

                                    }else {
                                        Toast.makeText(getApplicationContext(),task.getException().toString(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
            }

            @Override
            protected void onStart() {
                super.onStart();

                String userID = preferences.getString("userID", null);
                if (userID != null) {
                    Intent intent = new Intent(this, TypeDeBiensActivity.class);
                    startActivity(intent);
                }
            }
        }
    */


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

}