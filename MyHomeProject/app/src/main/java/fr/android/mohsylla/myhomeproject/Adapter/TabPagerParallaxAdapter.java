package fr.android.mohsylla.myhomeproject.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;



public class TabPagerParallaxAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> childFragments = new ArrayList<>();
    private ArrayList<String> titles = new ArrayList<>();

    public TabPagerParallaxAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return childFragments.get(position);
    }

    @Override
    public int getCount() {
        return childFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }


    public void addFragment(Fragment fragment, String title){
        childFragments.add(fragment);
        titles.add(title);
    }
}
