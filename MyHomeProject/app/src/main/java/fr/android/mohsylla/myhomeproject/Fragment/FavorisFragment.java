package fr.android.mohsylla.myhomeproject.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fr.android.mohsylla.myhomeproject.Activities.MainActivity;
import fr.android.mohsylla.myhomeproject.Activities.TypeDeBiensActivity;
import fr.android.mohsylla.myhomeproject.R;

import static fr.android.mohsylla.myhomeproject.R.id.bottomNavigationView;

/**
 * A simple {@link Fragment} subclass.
 *
 *
 * **/



public class FavorisFragment extends Fragment {

    Button mAddMaison;

    public FavorisFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_favoris, container, false);

   //    ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        final BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.bottomNavigationView);
   //  bottomNavigationView.getSelectedItemId()



mAddMaison = (Button) view.findViewById(R.id.buttonAddFavoris);
        mAddMaison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                MainFragment mainFragment = new MainFragment();
                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.main_container, mainFragment);
               // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

//              bottomNavigationView.setSelectedItemId(R.id.id_recherche);

            }
        });


        return view;
    }





  /*  @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }
 */

}
