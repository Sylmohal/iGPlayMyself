package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.LocalityActivity;
import fr.android.mohsylla.myhomeproject.Fragment.LocalityFragment;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by bios on 20/07/2017.
 */

public class BudgetAdapter extends RecyclerView.Adapter<BudgetAdapter.MyViewHolder> {
    Context context;
    ArrayList<String> data;
    LayoutInflater inflater;
    int selectedPosition = -1;

    private ArrayList<HomeModel> HomeData;
    private String PieceChoosi;


    public BudgetAdapter(Context context , ArrayList<HomeModel> HomeData,ArrayList<String> data,String choose){

        this.context = context;
        this.HomeData = HomeData;
        this.data = data;
        this.PieceChoosi = choose;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item_budget,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.tv_item_budget.setText(data.get(position));
        holder.radioButton.setChecked(position == selectedPosition);
        holder.tv_item_budget.setTag(position);
        holder.radioButton.setTag(position);
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (int) v.getTag();
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tv_item_budget;
        RadioButton radioButton;
        Context context;
        View mView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            tv_item_budget = (TextView) itemView.findViewById(R.id.tv_item_budget);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio_button);

            context = itemView.getContext();
            tv_item_budget.setOnClickListener(this);
            mView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();

            int position = getAdapterPosition();

            itemCheckChanged(v);


            bundle.putParcelableArrayList("homeData",HomeData);
            bundle.putString("budgetChoisi",data.get(position));
            bundle.putString("nombreDePiece",PieceChoosi);


            FragmentManager manager1 = ((AppCompatActivity)context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction1 = manager1.beginTransaction();
            LocalityFragment fragmentLocality = new LocalityFragment();
            fragmentTransaction1.replace(R.id.main_container, fragmentLocality);
            fragmentTransaction1.addToBackStack(null);
            fragmentTransaction1.commit();
            fragmentLocality.setArguments(bundle);






        }


    }
}
