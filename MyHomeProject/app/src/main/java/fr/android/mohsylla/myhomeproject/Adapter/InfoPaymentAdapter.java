package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.InfoPayment;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 12/05/2017.
 */

public class InfoPaymentAdapter extends RecyclerView.Adapter<InfoPaymentAdapter.InfoPaymentViewHolder> {


    private Context mContext;
    private ArrayList<InfoPayment> mList = new ArrayList<>();
    private HomeModel HomeData;

    public InfoPaymentAdapter(Context context, ArrayList<InfoPayment> data, HomeModel home){
        mContext = context;
        mList = data;
        this.HomeData = home;
    }

    @Override
    public InfoPaymentAdapter.InfoPaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InfoPaymentAdapter.InfoPaymentViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_info_payment, parent, false));
    }

    @Override
    public void onBindViewHolder(InfoPaymentAdapter.InfoPaymentViewHolder holder, int position) {
        InfoPayment infoPayment = mList.get(position);
        holder.tvTitle.setText(infoPayment.getTitle());

        switch (position){
            case 0: holder.tvPrice.setText(HomeData.getPrice()); break;
            case 1: holder.tvPrice.setText(HomeData.getCaution()); break;
            case 2: holder.tvPrice.setText(HomeData.getAvance());
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class InfoPaymentViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle , tvPrice;

        public InfoPaymentViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_payment);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);

        }
    }
}
