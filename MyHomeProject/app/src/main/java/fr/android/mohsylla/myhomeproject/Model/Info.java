package fr.android.mohsylla.myhomeproject.Model;

/**
 * Created by Mohsylla on 12/05/2017.
 */

public class Info {

        private String title;
        private int count;
    private int imageId;


    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }



        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }



