package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.AffichageDesMaisonActivity;
import fr.android.mohsylla.myhomeproject.Fragment.AffichageDesMaisonsFragment;
import fr.android.mohsylla.myhomeproject.Fragment.LocalityFragment;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 15/06/2017.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {

    Context context;
    ArrayList<HomeModel> data;
    LayoutInflater inflater;
    int selectedPosition = -1;

   public LocationAdapter(Context context , ArrayList<HomeModel> data){

       this.context = context;
       this.data = data;
       inflater = LayoutInflater.from(context);
   }

    @Override
    public LocationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.list_item_budget,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final LocationAdapter.MyViewHolder holder, final int position) {
        holder.tv_item_location.setText(data.get(position).getStreet());
        holder.radioButton.setChecked(position == selectedPosition);
        holder.tv_item_location.setTag(position);
        holder.radioButton.setTag(position);

//      holder.tv_item_header_location.setText(data.get(position).getHeader());
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (int) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tv_item_location , tv_item_header_location;
        RadioButton radioButton;
        Context context;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_location = (TextView) itemView.findViewById(R.id.tv_item_budget);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio_button);

            //tv_item_header_location = (TextView) itemView.findViewById(R.id.tv_item_header_location);
            context = itemView.getContext();
            tv_item_location.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
           Bundle bundle = new Bundle();
            int position = getAdapterPosition();
            itemCheckChanged(view);



            bundle.putParcelableArrayList("homeData",data);
            bundle.putString("localiteChoisi",data.get(position).getStreet());

            FragmentManager manager1 = ((AppCompatActivity)context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction1 = manager1.beginTransaction();
            AffichageDesMaisonsFragment fragmentAffichageMaisons = new AffichageDesMaisonsFragment();
            fragmentTransaction1.replace(R.id.main_container, fragmentAffichageMaisons);
            fragmentTransaction1.addToBackStack(null);
            fragmentTransaction1.commit();
            fragmentAffichageMaisons.setArguments(bundle);

        }

    }


}
