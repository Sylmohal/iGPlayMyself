package fr.android.mohsylla.myhomeproject.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import fr.android.mohsylla.myhomeproject.R;

public class RechercheMultiCriteresActivity extends AppCompatActivity {


    Button mButtonRechercher;
    EditText EditTypedeBiens , EditNombredePieces , EditBudget, EditLieu , EditNombredeChambres , EditNombredeSalon
            , EditNombredeSallesdeBain , EditNombredeCuisines , EditComodite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche_multi_criteres);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
           toolbar.setNavigationIcon(R.mipmap.deleteview_icon);





     setSupportActionBar(toolbar);
      getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        mButtonRechercher = (Button) findViewById(R.id.buttonRechercher);
        mButtonRechercher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

     EditTypedeBiens = (EditText) findViewById(R.id.id_type_de_biens);
        EditNombredePieces = (EditText) findViewById(R.id.id_nombre_de_pieces);
        EditBudget = (EditText) findViewById(R.id.id_choisir_budget_);
        EditLieu = (EditText) findViewById(R.id.id_choisir_localité);
        EditNombredeChambres = (EditText) findViewById(R.id.id_choisir_chambre_);
        EditNombredeSalon = (EditText) findViewById(R.id.id_choisir_salon_);
      EditNombredeSallesdeBain = (EditText) findViewById(R.id.id_choisir_salles_de_bain);
        EditNombredeCuisines = (EditText) findViewById(R.id.id_choisir_cuisine_);
        EditComodite = (EditText) findViewById(R.id.id_choisir_commodites_);



/*

        EditTypedeBiens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final android.app.FragmentManager fragmentManager = getFragmentManager();
                final NumberChoiceDialogFragment numberChoiceDialogFragment = new NumberChoiceDialogFragment();


                numberChoiceDialogFragment.show(fragmentManager,"oook");
            }
        }); */

/*
     EdTypedeBiens.setOnItemSelectedListener(this);
        List<String> categoriesTypedeBiens = new ArrayList<String>();
        categoriesTypedeBiens.add("Studio");
        categoriesTypedeBiens.add("Appartement");
        categoriesTypedeBiens.add("Villa");


        spinnerNombredePieces.setOnItemSelectedListener(this);
        List<String> categoriesNombredePieces = new ArrayList<String>();
        categoriesNombredePieces.add("2");
        categoriesNombredePieces.add("3");
        categoriesNombredePieces.add("4");
        categoriesNombredePieces.add("5");
        categoriesNombredePieces.add("6");
        categoriesNombredePieces.add("7 et Plus");


        spinnerBudget.setOnItemSelectedListener(this);
        List<String> categoriesBudget = new ArrayList<String>();
        categoriesBudget.add("80 000 - 100 000");
        categoriesBudget.add("100 000 - 200 000");
        categoriesBudget.add("200 000 - 300 000");
        categoriesBudget.add("300 000 - 400 000");
        categoriesBudget.add("400 000 - 600 000");
        categoriesBudget.add("600 000 - 1 000 000");


        spinnerLieu.setOnItemSelectedListener(this);
        List<String> categoriesLieu = new ArrayList<String>();
        categoriesLieu.add("Cocody");
        categoriesLieu.add("Yopougon");
        categoriesLieu.add("Adjame");
        categoriesLieu.add("Treichville");
        categoriesLieu.add("Marcory");
        categoriesLieu.add("PortBouet");


        spinnerNombredeChambres.setOnItemSelectedListener(this);
        List<String> categoriesChambres = new ArrayList<String>();
        categoriesChambres.add("1");
        categoriesChambres.add("2");
        categoriesChambres.add("3");
        categoriesChambres.add("4");
        categoriesChambres.add("5");
        categoriesChambres.add("6 et plus");

        spinnerNombredeSalon.setOnItemSelectedListener(this);
        List<String> categoriesSalon = new ArrayList<String>();
        categoriesSalon.add("1");
        categoriesSalon.add("2");
        categoriesSalon.add("3");
        categoriesSalon.add("4 et plus");



        spinnerNombredeSallesdeBain.setOnItemSelectedListener(this);
        List<String> categoriesSallesdeBain = new ArrayList<String>();
        categoriesSallesdeBain.add("1");
        categoriesSallesdeBain.add("2");
        categoriesSallesdeBain.add("3");
        categoriesSallesdeBain.add("4");
        categoriesSallesdeBain.add("5");
        categoriesSallesdeBain.add("6 et plus");


        spinnerNombredeCuisines.setOnItemSelectedListener(this);
        List<String> categoriesCuisines = new ArrayList<String>();
        categoriesCuisines.add("1");
        categoriesCuisines.add("2");
        categoriesCuisines.add("3 et plus");





       ArrayAdapter<String> dataAdapterTypedeBiens = new ArrayAdapter<String>(this, android.R.layout.preference_category, categoriesTypedeBiens);
        ArrayAdapter<String> dataAdapterNombredePieces = new ArrayAdapter<String>(this, android.R.layout.preference_category, categoriesNombredePieces);
        ArrayAdapter<String> dataAdapterBudget = new ArrayAdapter<String>(this, android.R.layout.preference_category, categoriesBudget);
        ArrayAdapter<String> dataAdapterLieu = new ArrayAdapter<String>(this, android.R.layout.preference_category, categoriesLieu);
        ArrayAdapter<String> dataAdapterChambres = new ArrayAdapter<String>(this, android.R.layout.preference_category, categoriesChambres);
        ArrayAdapter<String> dataAdapterSalon = new ArrayAdapter<String>(this, android.R.layout.preference_category, categoriesSalon);
        ArrayAdapter<String> dataAdapterSallesdeBain = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categoriesSallesdeBain);
        ArrayAdapter<String> dataAdapterCuisine = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categoriesCuisines);







        dataAdapterTypedeBiens.setDropDownViewResource(android.R.layout.preference_category);
        dataAdapterNombredePieces.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        dataAdapterBudget.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        dataAdapterLieu.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        dataAdapterChambres.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        dataAdapterSalon.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        dataAdapterSallesdeBain.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        dataAdapterCuisine.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);




       EditTypedeBiens.setAdapter(dataAdapterTypedeBiens);
        spinnerNombredePieces.setAdapter(dataAdapterNombredePieces);
        spinnerBudget.setAdapter(dataAdapterBudget);
        spinnerLieu.setAdapter(dataAdapterLieu);
        spinnerNombredeChambres.setAdapter(dataAdapterChambres);
        spinnerNombredeSallesdeBain.setAdapter(dataAdapterSallesdeBain);
        spinnerNombredeCuisines.setAdapter(dataAdapterCuisine);
        spinnerNombredeSalon.setAdapter(dataAdapterSalon);





*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

}
