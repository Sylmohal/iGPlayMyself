package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.CustomAdapterHouse;
import fr.android.mohsylla.myhomeproject.Datas.AmbergurView;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

public class AffichageDesMaisonActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CustomAdapterHouse adapterHouse;



    private String locatliteChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<HomeModel> DataFinaly;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_recycler_view);
/*
        // drawer layout
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true); */


        locatliteChoisi = getIntent().getStringExtra("localiteChoisi");
        HomeData = getIntent().getParcelableArrayListExtra("homeData");

        DataFinaly = new ArrayList<>();
        for (HomeModel item : HomeData) {
            int result = locatliteChoisi.compareTo(item.getStreet());
            if(result == 0){
                DataFinaly.add(item);
            }
        }

        //


        //

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        adapterHouse = new CustomAdapterHouse(this , DataFinaly);

        recyclerView.setAdapter(adapterHouse);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }




}
