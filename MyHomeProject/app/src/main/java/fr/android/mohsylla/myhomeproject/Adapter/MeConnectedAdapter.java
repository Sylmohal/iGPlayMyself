package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Fragment.LoginFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeConnectedFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.Model.InfoProfile;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 03/09/2017.
 */

public class MeConnectedAdapter extends RecyclerView.Adapter<MeConnectedAdapter.MyViewHolder> {

    Context context;
    ArrayList<InfoProfile> data;
    LayoutInflater inflater;


    public MeConnectedAdapter (Context context , ArrayList<InfoProfile> data){

        this.context= context;
        this.data = data;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public MeConnectedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_profile,parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MeConnectedAdapter.MyViewHolder holder, int position) {
        holder.imageView.setImageResource(data.get(position).getImageProfile());
        holder.txtProfile.setText(data.get(position).getTitre());

        holder.txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                MeFragment fragmentMe = new MeFragment();
                fragmentTransaction.replace(R.id.main_container, fragmentMe);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView imageView;
        TextView txtProfile;
        final Context context;



        public MyViewHolder(View itemView) {
            super(itemView);


            imageView = (ImageView) itemView.findViewById(R.id.txt_icon);
            txtProfile =  (TextView) itemView.findViewById(R.id.txt_profile);
            context = itemView.getContext();
        }
    }
}
