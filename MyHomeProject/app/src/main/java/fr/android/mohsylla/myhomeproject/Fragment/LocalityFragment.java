package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.SimpleDividerItemDecoration;
import fr.android.mohsylla.myhomeproject.Adapter.LocationAdapter;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocalityFragment extends Fragment {

    LocationAdapter adapter;
    private String budgetChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<String> LocalityList;
    private String NombreDePieceChoisi;
    private ArrayList<HomeModel> DataFinaly;

    public LocalityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_locality, container, false);


/*
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true); */

        Bundle bundle = getArguments();
        budgetChoisi =  bundle.getString("budgetChoisi");
        HomeData = bundle.getParcelableArrayList("homeData");
        NombreDePieceChoisi = bundle.getString("nombreDePiece");




        DataFinaly = new ArrayList<>();
        for (HomeModel item : HomeData ) {
            int result = NombreDePieceChoisi.compareTo(String.valueOf(item.getRooms()));
            if(result == 0){
                int resultSelonBudget = budgetChoisi.compareTo(item.getPrice());
                if(resultSelonBudget == 0){
                    Log.v("local",item.getPrice());
                    DataFinaly.add(item);
                }

            }
        }





        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);

        adapter = new LocationAdapter(getActivity(), DataFinaly);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));



        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        budgetChoisi =  bundle.getString("budgetChoisi");
        HomeData = bundle.getParcelableArrayList("homeData");
        NombreDePieceChoisi = bundle.getString("nombreDePiece");
    }




}
