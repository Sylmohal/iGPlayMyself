package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import fr.android.mohsylla.myhomeproject.Activities.SimpleDividerItemDecoration;
import fr.android.mohsylla.myhomeproject.Adapter.BudgetAdapter;
import fr.android.mohsylla.myhomeproject.Model.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetFragment extends Fragment {

    TextView mtitleBudget, mSubtitleBudget;



    BudgetAdapter adapter;



    private String nombreDePieceChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<String> BudgetList;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");


    public BudgetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_budget, container, false);
        mtitleBudget = (TextView) v.findViewById(R.id.titleBUDGET);




       /* Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Heavy.ttf");
        mtitleBudget.setTypeface(myCustomFont); */

        mSubtitleBudget = (TextView) v.findViewById(R.id.subtitleBUDGET);
        /*Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Book.ttf");
        mSubtitleBudget.setTypeface(myCustomFont1); */

        HomeData = new ArrayList<>();
        BudgetList = new ArrayList<>();

        Bundle bundle = getArguments();
        nombreDePieceChoisi =  bundle.getString("nombreDePieceChoisi");


      //  nombreDePieceChoisi = getActivity().getIntent().getStringExtra("nombreDePieceChoisi");

        final RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);

        int resultPiece = nombreDePieceChoisi.compareTo("1");
        if(resultPiece == 0) {
            listingsReference
                    .orderByChild("type")
                    .equalTo("Studio")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                        HomeModel home = child.getValue(HomeModel.class);
                                        home.setKey(child.getRef().getKey().toString());
                                        HomeData.add(home);
                                        BudgetList.add(home.getPrice());
                                        Log.v("ERROR", String.valueOf(HomeData.size()));
                                    }
                                    TrieForBudget();
                                    mRecyclerView.invalidate();

                                    adapter.notifyDataSetChanged();
                                }
                            });

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.v("ERROR", databaseError.toString());
                        }
                    });


        }else {
            Bundle bundleHome = getArguments();
            HomeData = bundleHome.getParcelableArrayList("homeData");

            for(HomeModel item : HomeData) {
                int result = nombreDePieceChoisi.compareTo(String.valueOf(item.getRooms()));
                if(result == 0){
                    BudgetList.add(item.getPrice());
                }
            }

            TrieForBudget();
        }





        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        adapter = new BudgetAdapter(getActivity() , HomeData,BudgetList,nombreDePieceChoisi);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);




        return v;
    }

    private void TrieForBudget()
    {
        Collections.sort(BudgetList);

        /*
        *
        * RECUPERATION DES BUDGETS DE MANIERE IRREPETITIF
         */
        Boolean condition = true;

        while (condition){
            condition = false;
            for(int i = 0;i < BudgetList.size();i++){
                if(i < BudgetList.size()-1){
                    int result = BudgetList.get(i).compareTo(BudgetList.get(i+1));
                    if(result == 0){
                        BudgetList.remove(i);
                        condition = true;
                    }
                }

            }
        }
    }
/*
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle bundle = getArguments();
        nombreDePieceChoisi =  bundle.getString("nombreDePieceChoisi");

        Bundle bundleHome = getArguments();
        HomeData = bundleHome.getParcelableArrayList("homeData");



            setRetainInstance(true);



    }
*/

}
