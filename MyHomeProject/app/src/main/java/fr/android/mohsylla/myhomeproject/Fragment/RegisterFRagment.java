package fr.android.mohsylla.myhomeproject.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import fr.android.mohsylla.myhomeproject.Activities.DossierOptionnelRegister;
import fr.android.mohsylla.myhomeproject.Activities.MainActivity;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFRagment extends Fragment {
    Button mRegister;
    public FirebaseAuth mAuth;
    private EditText mEmailEditText, mPasswordEditText, mNameEditText;
    SharedPreferences preferences;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    public RegisterFRagment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_register_fragment, container, false);

       // getSupportActionBar().setTitle(" ");
        // getSupportActionBar().hide();
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbarRegister);
toolbar.setNavigationIcon(R.mipmap.deleteview_icon);

      //  ((AppCompatActivity) getActivity()).getDelegate().setSupportActionBar(toolbar);

/*
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true); */



        mNameEditText = (EditText) view.findViewById(R.id.id_name_register);
        mEmailEditText = (EditText) view.findViewById(R.id.id_email_register);
        mPasswordEditText = (EditText) view.findViewById(R.id.id_password_register);

        preferences = getActivity().getSharedPreferences("ID", Context.MODE_PRIVATE);
        mAuth = FirebaseAuth.getInstance();

        mRegister = (Button) view.findViewById(R.id.buttonRegister);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                OptionelRegisterFragment fragmentOptionelRegister = new OptionelRegisterFragment();
                fragmentTransaction.replace(R.id.main_container, fragmentOptionelRegister);
                fragmentTransaction.commit();

            }
        });


        return view;
    }




}
