package fr.android.mohsylla.myhomeproject.Model;

/**
 * Created by Mohsylla on 12/05/2017.
 */

public class InfoPayment {


    private String title;
    private String price;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public InfoPayment(){

    }
}
