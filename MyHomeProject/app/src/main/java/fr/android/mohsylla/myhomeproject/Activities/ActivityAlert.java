package fr.android.mohsylla.myhomeproject.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import fr.android.mohsylla.myhomeproject.R;

public class ActivityAlert extends AppCompatActivity {

    int minteger = 1;
    Button minusBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);

        minusBtn = (Button) findViewById(R.id.decrease);
    }

    public void increaseInteger(View view) {
        minteger = minteger + 1;
        display(minteger);

    }public void decreaseInteger(View view) {
        minteger = minteger - 1;
        display(minteger);
    }

    private void display(int number) {
        TextView displayInteger = (TextView) findViewById(R.id.integer_number);
        if(number<0 || number == 0) {

            minusBtn.setVisibility(View.GONE);

        }
        else {
            minusBtn.setVisibility(View.VISIBLE);
        }

        displayInteger.setText("" + number);
    }
}
