package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.TypeDeBienAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataTypeDeBien;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
  /*  private TabLayout mTabLayout;
    private ViewPager mViewPager; */



    int counter = 0;

    TypeDeBienAdapter adapter;
    TextView mTypeDeBiens, mChoisirTypedeBiens;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");
    private ArrayList<HomeModel> HomeData;


    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState==null){
            counter=0;
        }
        else {
            counter=savedInstanceState.getInt("counter",0);
        }
    }

    // @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_search, container, false);

     //   ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


/*
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.setStatusBarColor(getActivity().getColor(R.color.white));
        }
*/


/*
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout_searchFragment);
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager_search_fragment);


        TabPagerParallaxAdapter mAdapter = new TabPagerParallaxAdapter(getFragmentManager());



        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager); */


     //   ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        mTypeDeBiens = (TextView) view.findViewById(R.id.titleTYPEDEBIENS);


        mChoisirTypedeBiens = (TextView) view.findViewById(R.id.titleCHOISISSEZTYPEDEBIENS);


        final RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        adapter = new TypeDeBienAdapter(getActivity(), DataTypeDeBien.getData(),"Location");


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        //set the adapter
        mRecyclerView.setAdapter(adapter);

      //  setHasOptionsMenu(true);

        return view;


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("counter",counter);

    }


}