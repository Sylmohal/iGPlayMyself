package fr.android.mohsylla.myhomeproject.Datas;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Model.InfoProfile;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 03/09/2017.
 */

public class DataProfileDeconnexion {

    public static ArrayList<InfoProfile> getData()


    {
        ArrayList<InfoProfile> data = new ArrayList<>();

        String[] Categories = {

                "Agrément" ,
                "A propos de MyHome",
                "Nous Contacter" ,
                "Faire des remarques",
                "Paramètres" ,
                "Se Deconnecter"

        };


        int[] images = {

                R.mipmap.agrement_icon,
                R.mipmap.aboutus_icons,
                R.mipmap.contacter,
                R.mipmap.remarque_icon,
                R.mipmap.setting_icon_copy,
                0


        };

        for (int i = 0 ; i <images.length ; i++)
        {
            InfoProfile current = new InfoProfile();
            current.setTitre(Categories[i]);
            current.setImageProfile(images[i]);
            data.add(current);
        }
        return data;
    }

}
