package fr.android.mohsylla.myhomeproject.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 04/10/2017.
 */

public class InfoStudioFragment extends Fragment  implements OnMapReadyCallback {

    private View rootView;
    public HomeModel Homedata;

    public HomeModel getHomedata() {
        return Homedata;
    }

    public void setHomedata(HomeModel homedata) {
        Homedata = homedata;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_info_studio, null);
        }
        TextView DescriptionTextView = (TextView) rootView.findViewById(R.id.id_description);
        DescriptionTextView.setText(Homedata.getDescription());


        MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(Double.valueOf(Homedata.getLatitude()), Double.valueOf(Homedata.getLongitude()));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,15);
        googleMap.moveCamera(cameraUpdate);
        googleMap.setMaxZoomPreference(15);
        googleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(300)
                .strokeColor(R.color.colorMyhome)
                .fillColor(Color.argb(39,141,130,206)));
    }
}

