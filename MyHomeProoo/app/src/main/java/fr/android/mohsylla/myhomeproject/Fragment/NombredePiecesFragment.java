package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import fr.android.mohsylla.myhomeproject.Adapter.AppartAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NombredePiecesFragment extends Fragment {

    TextView mNOMBREPIECES , mCHOISISSEZPIECES;
    AppartAdapter adapter;
    private int selectedRoom;
    private String typeDeBienChoisi;
    //  private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //  private NavigationView navigationView;
    private ArrayList<String> NomDePieceList;
    public  String possession;
    public ProgressBar homeProgressBar;
    public RecyclerView mRecyclerView;

    public NombredePiecesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_nombre_de_pieces, container, false);

        NomDePieceList = new ArrayList<>();

        Bundle bundle = getArguments();
        typeDeBienChoisi =  bundle.getString("typeDeBien");
        possession = bundle.getString("possession");

         mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
         homeProgressBar = (ProgressBar)view.findViewById(R.id.homeProgress);

        new NombreDePieceQueryTask().execute();


        return view;
    }

    public class NombreDePieceQueryTask extends AsyncTask<Void,Void,ArrayList<HomeModel>> {

        ArrayList<HomeModel> listHome;
        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {
       listHome = new ArrayList<>();
            DataBase.listingsHomeRef()
                    .child(possession)
                    .child(typeDeBienChoisi)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                homeProgressBar.setVisibility(View.GONE);
                                for(DataSnapshot child : dataSnapshot.getChildren()){
                                    HomeModel home = child.getValue(HomeModel.class);
                                    home.setHomeID(child.getRef().getKey().toString());
                                    home.setType(typeDeBienChoisi);
                                    home.setPossessionDuBien(possession);
                                    int result = home.getPiece().compareTo("enregistrement...");
                                    if(result != 0){
                                        listHome.add(home);
                                        NomDePieceList.add(home.getPiece());
                                    }
                                }
                                /*
                        * TRIE DANS L'ORDRE DECROISSANT DU NOMBRE DE PIECE
                         */
                                Collections.sort(NomDePieceList);
                        /*
                        * RECUPERATION DU NOMBRE DE PIECE IRREPETITIF
                         */

                                Boolean conditionDelete = true;
                                while (conditionDelete){
                                    conditionDelete = false;
                                    for(int i = 0;i < NomDePieceList.size();i++){
                                        if(i < NomDePieceList.size()-1){
                                            int result = NomDePieceList.get(i).compareTo(NomDePieceList.get(i+1));
                                            if(result == 0){
                                                NomDePieceList.remove(i);
                                                conditionDelete = true;
                                            }
                                        }

                                    }
                                }
                                mRecyclerView.invalidate();
                                adapter.notifyDataSetChanged();
                            } else {
                                homeProgressBar.setVisibility(View.GONE);
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.v("ERROR",databaseError.toString());
                        }
                    });

            return listHome;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> homeModels) {
            adapter = new AppartAdapter(getActivity(),NomDePieceList,homeModels);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
            //set the adapter
            mRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mNOMBREPIECES = (TextView) getActivity().findViewById(R.id.titlePIECES);
        mCHOISISSEZPIECES = (TextView) getActivity().findViewById(R.id.choisirNOMBREPIECES);

    }



}
