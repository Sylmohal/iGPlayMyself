package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import fr.android.mohsylla.myhomeproject.Model.Models.UserModel;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {

    Button mSeConnnecter , mEnregister;
    public FirebaseAuth mAuth;
    private EditText mEmailEditText,mPasswordEditText;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Fabric.with(this, new Crashlytics());
       // getSupportActionBar().setTitle("");
//        getSupportActionBar().hide();

        preferences = getSharedPreferences("ID", Context.MODE_PRIVATE);

        mSeConnnecter = (Button) findViewById(R.id.buttonSeConnecter);
        mEnregister = (Button) findViewById(R.id.buttonEnregistrer);
        mEmailEditText = (EditText) findViewById(R.id.id_email);
        mPasswordEditText = (EditText) findViewById(R.id.id_password);
        final ProgressBar loginProgression = (ProgressBar) findViewById(R.id.loginProgression);

        mAuth = FirebaseAuth.getInstance();

        mSeConnnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSeConnnecter.setText(" ");
                loginProgression.setVisibility(View.VISIBLE);

                if(mEmailEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Email Vide",Toast.LENGTH_SHORT).show();
                }
                if(mPasswordEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Password Vide",Toast.LENGTH_SHORT).show();
                }

                if(mEmailEditText.getText().length() != 0 && mPasswordEditText.getText().length() != 0){
                    mAuth.signInWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull final Task<AuthResult> task) {
                                    if(task.isSuccessful()){

                                        final SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("userID",task.getResult().getUser().getUid().toString());
                                        DataBase.userRef().addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                final UserModel userModel = dataSnapshot.getValue(UserModel.class);
                                                editor.putString("userName",userModel.getPrenom()+" "+userModel.getNom());
                                                editor.putString("userEmail",userModel.getEmail());
                                                editor.commit();
                                                Intent intent = new Intent();
                                                loginProgression.setVisibility(View.GONE);
                                                mSeConnnecter.setText("Connexion");
                                                setResult(0,intent);
                                                finish();
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });


                                    }else {
                                        loginProgression.setVisibility(View.GONE);
                                        mSeConnnecter.setText("Connexion");
                                        Toast.makeText(getApplicationContext(),task.getException().toString(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }


            }
        });
        mEnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("code 1",String.valueOf(resultCode));
        if(resultCode == 1){
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//
//        String userID = preferences.getString("userID",null);
//        if(userID != null){
//            Intent intent = new Intent(this,TypeDeBiensActivity.class);
//            startActivity(intent);
//        }
    }

    @Override
    public void onBackPressed() {
       finish();
    }
}
