package fr.android.mohsylla.myhomeproject.Activities;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.ListingsHomeAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Models.UserWishListModel;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;
import io.fabric.sdk.android.Fabric;

public class ListingsHomeActivity extends AppCompatActivity {


    RecyclerView recyclerView;


    private ArrayList<HomeModel> HomeData;
    private ArrayList<HomeModel> DataFinaly;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listings_home);
        Fabric.with(this, new Crashlytics());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        String locatliteChoisi = getIntent().getStringExtra("localiteChoisi");
        HomeData = getIntent().getParcelableArrayListExtra("homeData");

        DataFinaly = new ArrayList<>();
        for (HomeModel item : HomeData) {
            int result = locatliteChoisi.compareTo(item.getSecteur());
            if(result == 0){
                DataFinaly.add(item);
            }
        }

        HomeModel Data = DataFinaly.get(0);
        TextView tv_price = (TextView) findViewById(R.id.id_price);
        tv_price.setText(Data.getPrix()+" FCFA");

        TextView tv_localite = (TextView) findViewById(R.id.id_localite);
        tv_localite.setText(Data.getCommune()+" - "+Data.getSecteur());

        TextView tv_nbreDePiece = (TextView) findViewById(R.id.id_nombreDePiece);

        switch (Data.getPossessionDuBien()){
            case "Location":
                int result = "Studio".compareTo(Data.getType());
                if(result == 0){
                    tv_nbreDePiece.setText("Studio à louer");
                } else {

                    tv_nbreDePiece.setText(Data.getType()+" "+Data.getPiece()+"pieces à louer");
                }
                break;
            case "Vente":

                tv_nbreDePiece.setText(Data.getType()+" "+Data.getPiece()+" pieces à Vendre");
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        new CheckIfHomeIsLikedTask().execute(DataFinaly);
    }

    public class CheckIfHomeIsLikedTask extends AsyncTask<ArrayList<HomeModel>,Void,ArrayList<HomeModel>> {

        ListingsHomeAdapter adapterHouse;
        ArrayList<HomeModel> list;
        @Override
        protected ArrayList<HomeModel> doInBackground(ArrayList<HomeModel>... homeList) {
            list = homeList[0];
            adapterHouse = new ListingsHomeAdapter(ListingsHomeActivity.this , list);
            for(final HomeModel home : list){
                if(AuthService.CheckIfUserIsConnected(getApplicationContext())){
                    DataBase.userWishList().child(home.getHomeID()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                home.setLike(true);
                                UserWishListModel userWishListModel = dataSnapshot.getValue(UserWishListModel.class);
                                switch (userWishListModel.getState()){
                                    case "visite":
                                        home.setState("visite");
                                        break;
                                    case  "wish":
                                        home.setState("wish");
                                        break;
                                }
                                adapterHouse.notifyDataSetChanged();
                                recyclerView.invalidate();
                            }else {
                                home.setLike(false);
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

            }
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> homeModels) {
            super.onPostExecute(homeModels);
            adapterHouse = new ListingsHomeAdapter(ListingsHomeActivity.this , homeModels);
            recyclerView.setAdapter(adapterHouse);
            recyclerView.setLayoutManager(new LinearLayoutManager(ListingsHomeActivity.this));
        }
    }
}
