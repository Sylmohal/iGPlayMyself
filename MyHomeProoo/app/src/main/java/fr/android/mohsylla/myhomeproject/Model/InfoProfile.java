package fr.android.mohsylla.myhomeproject.Model;

/**
 * Created by Mohsylla on 03/08/2017.
 */

public class InfoProfile {


    public int imageProfile;
    public String titre;

    public int getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(int imageProfile) {
        this.imageProfile = imageProfile;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}
