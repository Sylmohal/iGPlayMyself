package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.LoginActivity;
import fr.android.mohsylla.myhomeproject.Activities.ShowHomeDetailsActivity;
import fr.android.mohsylla.myhomeproject.Fragment.FavorisFragment;
import fr.android.mohsylla.myhomeproject.Model.FavorisModel;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Models.UserWishListModel;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 26/09/2017.
 */

public class FavorisAdapter extends RecyclerView.Adapter<FavorisAdapter.MyViewHolder> {

    FavorisFragment context;
    ArrayList<HomeModel> data;
    LayoutInflater inflater;

    public FavorisAdapter(FavorisFragment context, ArrayList<HomeModel> data) {

        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context.getActivity());


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.list_item_favoris, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        HomeModel homeModel = data.get(position);
        myViewHolder.SetImage(homeModel.getImagePrincipale().getPath());
        myViewHolder.SetInfoTextView(homeModel);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        private final Context context;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        private TextView price_tv,localite_tv,piece_tv;
        private Button likeBtn;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imgHome);
            context = itemView.getContext();

            likeBtn = (Button) itemView.findViewById(R.id.likeButton);
            price_tv = (TextView)itemView.findViewById(R.id.id_price);
            piece_tv = (TextView) itemView.findViewById(R.id.id_nombreDePiece);
            localite_tv = (TextView) itemView.findViewById(R.id.id_localite);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent; intent =  new Intent(context.getApplicationContext(), ShowHomeDetailsActivity.class);
                    intent.putExtra("homeData",data.get(position));
                    context.startActivity(intent);
                }
            });

            likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HandleLikeHome((Button)view,getAdapterPosition());
                }

            });
        }

        public void SetInfoTextView(HomeModel homeModel){
            switch (homeModel.getPossessionDuBien()){
                case "Location":
                    int result = "Studio".compareTo(homeModel.getType());
                    if(result == 0){
                        piece_tv.setText("Studio à louer");
                    } else {

                        piece_tv.setText(homeModel.getType()+" "+homeModel.getPiece()+"pieces à louer");
                    }
                    break;
                case "Vente":

                    piece_tv.setText(homeModel.getType()+" "+homeModel.getPiece()+" pieces à Vendre");
            }
            localite_tv.setText(homeModel.getCommune()+" - "+homeModel.getSecteur());
            price_tv.setText(homeModel.getPrix()+" FCFA");


            switch (homeModel.getState()){
                case "wish":
                    likeBtn.setBackground(context.getDrawable(R.mipmap.likehome_wish_icon));
                    break;
                case "visite":
                    likeBtn.setBackground(context.getDrawable(R.mipmap.likehome_visite_icon));
                    break;
                default:break;
            }

        }
        public void HandleLikeHome(final Button btn, final int position){
                final HomeModel homeModel = data.get(position);
                if(homeModel.getLike()){
                    switch (homeModel.getState()){
                        case "visite":
                            CancleVisitation(homeModel,btn);
                            break;
                        case "wish":
                            DataBase.userWishList().child(homeModel.getHomeID()).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if(databaseError == null){
                                        notifyDataSetChanged();
                                    }

                                }
                            });
                            break;
                        default:
                            break;
                    }
                }
        }
        public void CancleVisitation(final HomeModel home,final Button btn){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Visitation");
            builder.setMessage("Voulez vous annuler la visite ?");
            builder.setCancelable(false);

            builder.setPositiveButton("non", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.setNegativeButton("oui", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    DataBase.visiteRef().child(home.getHomeID()).removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError == null){
                                home.setState("wish");
                                btn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                                DataBase.userWishList().child(home.getHomeID()).child("state").setValue("wish");
                            }
                        }
                    });
                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        public void SetImage(String path){
            StorageReference storageReference = storage.getReference(path);
            Glide.with(context.getApplicationContext())
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(imageView);
        }
    }
}