package fr.android.mohsylla.myhomeproject.Model.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageModel implements Parcelable {
    private String path;
    private String downloadURL;

    public ImageModel(){}
    public ImageModel(String path, String downloadURL) {
        this.path = path;
        this.downloadURL = downloadURL;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDownloadURL() {
        return downloadURL;
    }


    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.path);
        dest.writeString(this.downloadURL);
    }

    protected ImageModel(Parcel in) {
        this.path = in.readString();
        this.downloadURL = in.readString();
    }

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel source) {
            return new ImageModel(source);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };
}
