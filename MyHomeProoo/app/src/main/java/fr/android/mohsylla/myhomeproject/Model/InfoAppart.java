package fr.android.mohsylla.myhomeproject.Model;

/**
 * Created by Mohsylla on 11/07/2017.
 */

public class InfoAppart {

    private int rooms;

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }
}
