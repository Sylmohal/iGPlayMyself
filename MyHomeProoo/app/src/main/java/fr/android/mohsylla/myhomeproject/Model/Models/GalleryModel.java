package fr.android.mohsylla.myhomeproject.Model.Models;

/**
 * Created by bios on 21/07/2017.
 */

public class GalleryModel {
    private String name;
    private String path;
    private String propid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPropid() {
        return propid;
    }

    public void setPropid(String propid) {
        this.propid = propid;
    }
}
