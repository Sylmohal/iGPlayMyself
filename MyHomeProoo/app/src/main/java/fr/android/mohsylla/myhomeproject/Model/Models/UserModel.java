package fr.android.mohsylla.myhomeproject.Model.Models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by bios on 07/10/2017.
 */
@IgnoreExtraProperties
public class UserModel {
    private String nom;
    private String prenom;
    private String email;
    private String salaire;
    private String profession;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalaire() {
        return salaire;
    }

    public void setSalaire(String salaire) {
        this.salaire = salaire;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public UserModel() {
    }

    public UserModel(String nom, String prenom, String email, String salaire, String profession) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.salaire = salaire;
        this.profession = profession;
    }
}
