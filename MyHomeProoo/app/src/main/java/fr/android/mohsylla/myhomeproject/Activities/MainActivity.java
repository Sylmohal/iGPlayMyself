package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;

import fr.android.mohsylla.myhomeproject.Fragment.FavorisFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MainFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeConnectedFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.R;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    Toolbar toolbar;

    private FragmentTransaction fragmentTransaction;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    public MainFragment mainFragment ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_container);
        if(fragment == null){
            fragment = new MainFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.main_container,fragment);
            transaction.commit();
        }

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setBottom(1);

    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpBottomNavigationView();
    }
    public void setUpBottomNavigationView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.id_recherche:
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new MainFragment()).addToBackStack("Frag1").commit();
                        item.setChecked(true);
                        break;

                    case R.id.id_wishlist:
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new FavorisFragment()).addToBackStack(null).commit();
                        item.setChecked(true);
                        break;

                    case R.id.id_me:

                        if (AuthService.CheckIfUserIsConnected(MainActivity.this)) {
                            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new MeConnectedFragment()).addToBackStack(null).commit();
                            item.setChecked(true);
                        }else{
                            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new MeFragment()).addToBackStack(null).commit();
                            item.setChecked(true);
                        }

                        break;
                }
                return true;

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 0){
            bottomNavigationView.setSelectedItemId(R.id.id_me);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.v("BAK",String.valueOf(getFragmentManager().getBackStackEntryCount()));

    }
}
