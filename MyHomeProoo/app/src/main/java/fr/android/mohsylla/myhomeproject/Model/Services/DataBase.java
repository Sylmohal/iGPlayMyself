package fr.android.mohsylla.myhomeproject.Model.Services;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by bios on 26/09/2017.
 */

public class DataBase {

    public static DatabaseReference userRef () {
        return FirebaseDatabase.getInstance().getReference().child("Users").child(AuthService.getUserID());
    }

    public static DatabaseReference listingsHomeRef () {
        return FirebaseDatabase.getInstance().getReference().child("home-listings");
    }

    public static DatabaseReference userWishList () {
        return FirebaseDatabase.getInstance().getReference().child("users-wishlist").child(AuthService.getUserID());
    }

    public static DatabaseReference notificationRef () {
        return FirebaseDatabase.getInstance().getReference().child("notification-listings").child(AuthService.getUserID());
    }

    public static DatabaseReference visiteRef () {
        return FirebaseDatabase.getInstance().getReference().child("visite-listings").child(AuthService.getUserID());
    }

}
