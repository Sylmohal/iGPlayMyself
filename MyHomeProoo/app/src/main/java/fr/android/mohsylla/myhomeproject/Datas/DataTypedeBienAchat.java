package fr.android.mohsylla.myhomeproject.Datas;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Model.InfoMain;

/**
 * Created by Mohsylla on 01/09/2017.
 */

public class DataTypedeBienAchat {


    public static ArrayList<InfoMain> getData() {


        ArrayList<InfoMain> data = new ArrayList<>();


        String[] Categories = {
                "Appartement",
                "Villa",
        };

        for (int i = 0 ; i <Categories.length ; i++)
        {
            InfoMain current = new InfoMain();
            current.setTitle(Categories[i]);
            data.add(current);
        }
        return data;
    }

}
