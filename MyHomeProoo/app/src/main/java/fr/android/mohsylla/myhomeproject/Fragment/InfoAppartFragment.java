package fr.android.mohsylla.myhomeproject.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.InfoAppartAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataInfoFragment;
import fr.android.mohsylla.myhomeproject.Model.Info;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 04/10/2017.
 */

public class InfoAppartFragment extends Fragment implements OnMapReadyCallback {


    private View rootView;
    private RecyclerView mRecyclerView;
    InfoAppartAdapter adapter;
    public HomeModel Homedata;


    public HomeModel getHomedata() {
        return Homedata;
    }

    public void setHomedata(HomeModel homedata) {
        Homedata = homedata;
    }

    private ArrayList<Info> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_info_appart, null);
            initialize();
        }
        TextView DescriptionTextView = (TextView)rootView.findViewById(R.id.id_description);
        DescriptionTextView.setText(Homedata.getDescription());

        return rootView;
    }

    private void initialize() {

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));

        adapter = new InfoAppartAdapter(getActivity(),Homedata);

        mRecyclerView.setAdapter(adapter);

        MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(Double.valueOf(Homedata.getLatitude()), Double.valueOf(Homedata.getLongitude()));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,15);
        googleMap.moveCamera(cameraUpdate);
        googleMap.setMaxZoomPreference(15);
        googleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(300)
                .strokeColor(R.color.colorMyhome)
                .fillColor(Color.argb(39,141,130,206)));
    }
}
