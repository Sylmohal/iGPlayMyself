package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.TypeDeBienAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataTypedeBienAchat;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragmentAchat extends Fragment {
    TypeDeBienAdapter adapter;
    TextView mTypeDeBiens, mChoisirTypedeBiens;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");
    private ArrayList<HomeModel> HomeData;


    public SearchFragmentAchat() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_search, container, false);


        mTypeDeBiens = (TextView) view.findViewById(R.id.titleTYPEDEBIENS);


        mChoisirTypedeBiens = (TextView) view.findViewById(R.id.titleCHOISISSEZTYPEDEBIENS);


        final RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        adapter = new TypeDeBienAdapter(getActivity(), DataTypedeBienAchat.getData(),"Vente");


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        //set the adapter
        mRecyclerView.setAdapter(adapter);

        //  setHasOptionsMenu(true);

        return view;



    }

}
