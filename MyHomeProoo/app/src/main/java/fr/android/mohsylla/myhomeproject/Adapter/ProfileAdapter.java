package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.LoginActivity;
import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.Model.InfoProfile;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 03/09/2017.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder> {

    FragmentActivity activity;
    ArrayList<InfoProfile> data;
    LayoutInflater inflater;

    public ProfileAdapter(FragmentActivity context , ArrayList<InfoProfile> data){

        this.activity= context;
        this.data = data;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public ProfileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_profile,parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.MyViewHolder holder, int position) {
        holder.imageView.setImageResource(data.get(position).getImageProfile());
        holder.txtProfile.setText(data.get(position).getTitre());

        if(position == 5){
            if(AuthService.CheckIfUserIsConnected(activity)){
                holder.txtProfile.setText("Deconnexion");
            }else {
                holder.txtProfile.setText("Connexion");
            }
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        ImageView imageView;
        TextView txtProfile;
        final Context context;


        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.txt_icon);
            txtProfile =  (TextView) itemView.findViewById(R.id.txt_profile);
            context = itemView.getContext();

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (getAdapterPosition()){

                case 5: // CONNEXION OR DECONNEXION
                    if(AuthService.CheckIfUserIsConnected(activity)){
                    AuthService.Deconnexion(activity);
                    FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = manager.beginTransaction();
                    MeFragment fragmentMe = new MeFragment();
                    fragmentTransaction.replace(R.id.main_container, fragmentMe);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    }else {
                        Intent intent =  new Intent(context.getApplicationContext(), LoginActivity.class);
                        activity.startActivityForResult(intent,0);
                    }


            }
        }
    }

}
