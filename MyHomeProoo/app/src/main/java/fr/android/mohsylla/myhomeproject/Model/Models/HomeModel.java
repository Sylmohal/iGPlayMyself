package fr.android.mohsylla.myhomeproject.Model.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

/**
 * Created by Mohsylla on 11/07/2017.
 */
@IgnoreExtraProperties
public class HomeModel implements Parcelable {

    private String douche;
    private String chambre;
    private String commune;
    private String description;
    private String cuisine;
    private String salon;
    private ImageModel imagePrincipale;
    private ArrayList<ImageModel> imageGallerie;
    private String longitude;
    private String latitude;
    private String piece;
    private String prix;
    private String niveau;
    private String superficie;
    private String secteur;
    private String type;
    private String possessionDuBien;
    private String homeID;
    private Boolean isLike = false;
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getLike() {
        return isLike;
    }

    public void setLike(Boolean like) {
        isLike = like;
    }

    public String getPossessionDuBien() {
        return possessionDuBien;
    }

    public void setPossessionDuBien(String possessionDuBien) {
        this.possessionDuBien = possessionDuBien;
    }

    public HomeModel(){}
    public HomeModel(String douche, String chambre, String commune, String description, String cuisine, String salon, ImageModel imagePrincipale, ArrayList<ImageModel> imageGallerie, String longitude, String latitude, String piece, String prix, String niveau, String superficie, String secteur, String type, String homeID) {
        this.douche = douche;
        this.chambre = chambre;
        this.commune = commune;
        this.description = description;
        this.cuisine = cuisine;
        this.salon = salon;
        this.imagePrincipale = imagePrincipale;
        this.imageGallerie = imageGallerie;
        this.longitude = longitude;
        this.latitude = latitude;
        this.piece = piece;
        this.prix = prix;
        this.niveau = niveau;
        this.superficie = superficie;
        this.secteur = secteur;
        this.type = type;
        this.homeID = homeID;
    }

    public String getDouche() {
        return douche;
    }

    public void setDouche(String douche) {
        this.douche = douche;
    }

    public String getChambre() {
        return chambre;
    }

    public void setChambre(String chambre) {
        this.chambre = chambre;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public ImageModel getImagePrincipale() {
        return imagePrincipale;
    }

    public void setImagePrincipale(ImageModel imagePrincipale) {
        this.imagePrincipale = imagePrincipale;
    }

    public ArrayList<ImageModel> getImageGallerie() {
        return imageGallerie;
    }

    public void setImageGallerie(ArrayList<ImageModel> imageGallerie) {
        this.imageGallerie = imageGallerie;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getSuperficie() {
        return superficie;
    }

    public void setSuperficie(String superficie) {
        this.superficie = superficie;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHomeID() {
        return homeID;
    }

    public void setHomeID(String homeID) {
        this.homeID = homeID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.douche);
        dest.writeString(this.chambre);
        dest.writeString(this.commune);
        dest.writeString(this.description);
        dest.writeString(this.cuisine);
        dest.writeString(this.salon);
        dest.writeParcelable(this.imagePrincipale, flags);
        dest.writeTypedList(this.imageGallerie);
        dest.writeString(this.longitude);
        dest.writeString(this.latitude);
        dest.writeString(this.piece);
        dest.writeString(this.prix);
        dest.writeString(this.niveau);
        dest.writeString(this.superficie);
        dest.writeString(this.secteur);
        dest.writeString(this.type);
        dest.writeString(this.possessionDuBien);
        dest.writeString(this.homeID);
        dest.writeValue(this.isLike);
        dest.writeString(this.state);
    }

    protected HomeModel(Parcel in) {
        this.douche = in.readString();
        this.chambre = in.readString();
        this.commune = in.readString();
        this.description = in.readString();
        this.cuisine = in.readString();
        this.salon = in.readString();
        this.imagePrincipale = in.readParcelable(ImageModel.class.getClassLoader());
        this.imageGallerie = in.createTypedArrayList(ImageModel.CREATOR);
        this.longitude = in.readString();
        this.latitude = in.readString();
        this.piece = in.readString();
        this.prix = in.readString();
        this.niveau = in.readString();
        this.superficie = in.readString();
        this.secteur = in.readString();
        this.type = in.readString();
        this.possessionDuBien = in.readString();
        this.homeID = in.readString();
        this.isLike = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.state = in.readString();
    }

    public static final Creator<HomeModel> CREATOR = new Creator<HomeModel>() {
        @Override
        public HomeModel createFromParcel(Parcel source) {
            return new HomeModel(source);
        }

        @Override
        public HomeModel[] newArray(int size) {
            return new HomeModel[size];
        }
    };
}
