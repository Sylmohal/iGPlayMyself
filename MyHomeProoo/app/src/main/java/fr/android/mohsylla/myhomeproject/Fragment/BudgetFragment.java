package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.utils.SimpleDividerItemDecoration;
import fr.android.mohsylla.myhomeproject.Adapter.BudgetAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetFragment extends Fragment {

    TextView mtitleBudget, mSubtitleBudget;

    BudgetAdapter adapter;
    public ProgressBar homeProgress;
    public RecyclerView mRecyclerView;

    private String nombreDePieceChoisi;
    private ArrayList<HomeModel> HomeData;
    public ArrayList<String> BudgetList;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");


    public BudgetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_budget, container, false);
        mtitleBudget = (TextView) v.findViewById(R.id.titleBUDGET);


        mSubtitleBudget = (TextView) v.findViewById(R.id.subtitleBUDGET);
        HomeData = new ArrayList<>();
        BudgetList = new ArrayList<>();

        Bundle bundle = getArguments();
        nombreDePieceChoisi =  bundle.getString("nombreDePieceChoisi");

         mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
         homeProgress = (ProgressBar) v.findViewById(R.id.homeProgress);

        int resultPiece = nombreDePieceChoisi.compareTo("1");
        if(resultPiece == 0) {

            new StudioBudgetTask().execute();

        }else {

            homeProgress.setVisibility(View.GONE);
            Bundle bundleHome = getArguments();
            HomeData = bundleHome.getParcelableArrayList("homeData");

            for(HomeModel item : HomeData) {
                int result = nombreDePieceChoisi.compareTo(String.valueOf(item.getPiece()));
                if(result == 0){
                    BudgetList.add(item.getPrix());
                }
            }

            TrieForBudget();
            mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
            adapter = new BudgetAdapter(getActivity() , HomeData,BudgetList,nombreDePieceChoisi);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(adapter);
        }


        return v;
    }

    private void TrieForBudget()
    {
        Collections.sort(BudgetList);
        /*
        * RECUPERATION DES BUDGETS DE MANIERE IRREPETITIF
         */
        Boolean condition = true;

        while (condition){
            condition = false;
            for(int i = 0;i < BudgetList.size();i++){
                if(i < BudgetList.size()-1){
                    int result = BudgetList.get(i).compareTo(BudgetList.get(i+1));
                    if(result == 0){
                        BudgetList.remove(i);
                        condition = true;
                    }
                }

            }
        }
    }

    public class StudioBudgetTask extends AsyncTask<Void,Void,ArrayList<HomeModel>> {

        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {
            final ArrayList<HomeModel> list = new ArrayList<>();
            DataBase.listingsHomeRef()
                    .child("Location")
                    .child("Studio")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                homeProgress.setVisibility(View.GONE);

                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    HomeModel home = child.getValue(HomeModel.class);
                                    home.setHomeID(child.getRef().getKey().toString());
                                    home.setPossessionDuBien("Location");
                                    home.setType("Studio");
                                    int result = home.getPiece().compareTo("enregistrement...");
                                    if(result != 0){
                                        list.add(home);
                                        BudgetList.add(home.getPrix());
                                    }

                                }

                                TrieForBudget();
                                mRecyclerView.invalidate();
                                adapter.notifyDataSetChanged();

                            }else {
                                homeProgress.setVisibility(View.GONE);
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.v("ERROR", databaseError.toString());
                        }
                    });
            return  list;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> homeList) {
            mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
            adapter = new BudgetAdapter(getActivity() , homeList,BudgetList,"1");
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(adapter);
        }
    }

}
