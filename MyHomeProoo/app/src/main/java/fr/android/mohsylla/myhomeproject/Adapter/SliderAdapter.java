package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Model.Models.ImageModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 06/09/2017.
 */

public class SliderAdapter extends PagerAdapter {




//    public int[] images = {R.drawable.o3, R.drawable.o4};

    public ArrayList<ImageModel> images;
    Context context;
    LayoutInflater layoutInflater;

    FirebaseStorage storage = FirebaseStorage.getInstance();


    public SliderAdapter(Context context, ArrayList<ImageModel> galleriList){
        this.context=context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = galleriList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = layoutInflater.inflate(R.layout.swipe_layout, container, false);

        ImageView imageview = (ImageView) itemView.findViewById(R.id.image_swipe);
        StorageReference storageReference = storage.getReference(images.get(position).getPath());
        Glide.with(context.getApplicationContext())
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .into(imageview);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
