package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Fragment.BudgetFragment;
import fr.android.mohsylla.myhomeproject.Fragment.NombredePiecesFragment;
import fr.android.mohsylla.myhomeproject.Model.InfoMain;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 15/05/2017.
 */

public class TypeDeBienAdapter extends RecyclerView.Adapter<TypeDeBienAdapter.MyViewHolder> {


    Context context;
    ArrayList<InfoMain> data;
    LayoutInflater inflater;
    OnItemClickListener mListener;
    String possessionDuBien;


    public TypeDeBienAdapter(Context context, ArrayList<InfoMain> data,String possessionDuBien) {

        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.possessionDuBien = possessionDuBien;

    }

    @Override
    public TypeDeBienAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final TypeDeBienAdapter.MyViewHolder holder, int position) {
        holder.tv_item_name.setText(data.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_item_name;
        View tapInterceptor;
        final Context context;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_name);
            tapInterceptor = itemView.findViewById(R.id.tap_interceptor);
            context = itemView.getContext();
            tv_item_name.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
           //final Intent intent;
            Bundle bundle = new Bundle();
            int position = getAdapterPosition();

            if (data.get(position).getTitle() == "Studio"){
                BudgetFragment fragmentBudget = new BudgetFragment();
                bundle.putString("nombreDePieceChoisi", "1");
                fragmentBudget.setArguments(bundle);
                ((AppCompatActivity)context)
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_out_right, android.R.anim.slide_in_left)
                        .replace(R.id.main_container,fragmentBudget)
                        .addToBackStack(null)
                        .commit();

            } else  {

                bundle.putString("typeDeBien", data.get(position).getTitle());
                bundle.putString("possession",possessionDuBien);
                NombredePiecesFragment fragmentPieces = new NombredePiecesFragment();
                fragmentPieces.setArguments(bundle);
                ((AppCompatActivity)context)
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_out_right, android.R.anim.slide_in_left)
                        .replace(R.id.main_container,fragmentPieces)
                        .addToBackStack(null)
                        .commit();

                //   intent.putExtra("typeDeBien",data.get(position).getTitle());
            }

//          switch (position) {
//
//                case 0:
//
//
//
//                    break;
//
//                case 1:
//                   // intent = new Intent(context, NombreDePieceActivity.class);
//
//                    bundle.putString("typeDeBien", "Appartement");
//                    bundle.putString("possession",possessionDuBien);
//
//                    FragmentManager manager1 = ((AppCompatActivity)context).getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction1 = manager1.beginTransaction();
//
//                    NombredePiecesFragment fragmentPieces1 = new NombredePiecesFragment();
//                    fragmentTransaction1.replace(R.id.main_container, fragmentPieces1);
//                    fragmentTransaction1.addToBackStack(null);
//                    fragmentTransaction1.commit();
//
//
//                    fragmentPieces1.setArguments(bundle);
//
//
//                  //  intent.putExtra("typeDeBien","Appartement");
//                    break;
//
//                default:
//
//
//                    break;
//
//            }
         //  context.startActivity(intent);
        }
    }


    public void setOnItemClickListener(OnItemClickListener listener){
        //assign the instance to your private variable
        mListener = listener;
    }


    public interface OnItemClickListener {
        //create your interface class for the item click listener
        // you can decide what item you want to return when an item is clicked
        //for this example, let's just return the item value (String)

        void onClick(InfoMain item);
    }
}
