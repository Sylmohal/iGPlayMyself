package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fr.android.mohsylla.myhomeproject.Adapter.LocalityAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.LocaliteModel;
import fr.android.mohsylla.myhomeproject.utils.SimpleDividerItemDecoration;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocalityFragment extends Fragment {

    LocalityAdapter adapter;
    private String budgetChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<String> LocalityList;
    private String NombreDePieceChoisi;
    private ArrayList<HomeModel> DataFinaly;


    ArrayList<LocaliteModel> locationHome = new ArrayList<>();
    Map<String,ArrayList<String>> locationData = new HashMap<>();

    public LocalityFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_locality, container, false);

/*
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true); */

        Bundle bundle = getArguments();
        budgetChoisi =  bundle.getString("budgetChoisi");
        HomeData = bundle.getParcelableArrayList("homeData");
        NombreDePieceChoisi = bundle.getString("nombreDePiece");




        DataFinaly = new ArrayList<>();
        for (HomeModel item : HomeData ) {
            int result = NombreDePieceChoisi.compareTo(String.valueOf(item.getPiece()));
            if(result == 0){
                int resultSelonBudget = budgetChoisi.compareTo(item.getPrix());
                if(resultSelonBudget == 0){
                    DataFinaly.add(item);
                }

            }
        }

        for (HomeModel item : DataFinaly){
            locationData.put(item.getCommune(),null);
        }

        for(String commune : locationData.keySet()){
            ArrayList<String> sect = new ArrayList<>();
            for (HomeModel item : DataFinaly){
                int result = commune.compareTo(item.getCommune());
                if(result == 0){
                    sect.add(item.getSecteur());
                    locationData.put(commune,sect);
                }
            }
        }

        for(String commune: locationData.keySet()){
            ArrayList<String> secteurs = locationData.get(commune);
            Collections.sort(secteurs);
            Boolean conditionDelete = true;
            while (conditionDelete){
                conditionDelete = false;
                for(int i = 0;i < secteurs.size();i++){
                    if(i < secteurs.size()-1){
                        int result = secteurs.get(i).compareTo(secteurs.get(i+1));
                        if(result == 0){
                            secteurs.remove(i);
                            conditionDelete = true;
                        }
                    }

                }
            }
            locationHome.add(new LocaliteModel(commune,secteurs));
        }

        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.location_section_recycleView);

        adapter = new LocalityAdapter(getActivity(), locationHome, DataFinaly);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);



        return v;
    }













}
