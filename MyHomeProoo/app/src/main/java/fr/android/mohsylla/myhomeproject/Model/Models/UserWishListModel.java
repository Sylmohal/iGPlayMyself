package fr.android.mohsylla.myhomeproject.Model.Models;

/**
 * Created by bios on 02/10/2017.
 */

public class UserWishListModel {
    private String typeDuBien;
    private String state;
    private String possessionDuBien;

    public UserWishListModel() {
    }

    public String getTypeDuBien() {
        return typeDuBien;
    }

    public void setTypeDuBien(String typeDuBien) {
        this.typeDuBien = typeDuBien;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPossessionDuBien() {
        return possessionDuBien;
    }

    public void setPossessionDuBien(String possessionDuBien) {
        this.possessionDuBien = possessionDuBien;
    }

    public UserWishListModel(String typeDuBien, String state, String possessionDuBien) {
        this.typeDuBien = typeDuBien;
        this.state = state;
        this.possessionDuBien = possessionDuBien;
    }
}
