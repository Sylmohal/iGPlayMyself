package fr.android.mohsylla.myhomeproject.Model;

/**
 * Created by Mohsylla on 09/06/2017.
 */

public class InfoGallery {

    private int imageId;

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
