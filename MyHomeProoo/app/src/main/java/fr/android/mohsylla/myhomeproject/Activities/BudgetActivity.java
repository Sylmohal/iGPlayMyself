package fr.android.mohsylla.myhomeproject.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import fr.android.mohsylla.myhomeproject.Adapter.BudgetAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;
import fr.android.mohsylla.myhomeproject.utils.SimpleDividerItemDecoration;

public class BudgetActivity extends AppCompatActivity {
TextView mtitleBudget, mSubtitleBudget;



    BudgetAdapter adapter;



    private String nombreDePieceChoisi;
    private ArrayList<HomeModel> HomeData;
    private ArrayList<String> BudgetList;

    private DatabaseReference listingsReference = FirebaseDatabase.getInstance().getReference().child("listings");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);



        mtitleBudget = (TextView) findViewById(R.id.titleBUDGET);
       /* Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Heavy.ttf");
        mtitleBudget.setTypeface(myCustomFont); */

        mSubtitleBudget = (TextView) findViewById(R.id.subtitleBUDGET);
        /*Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Book.ttf");
        mSubtitleBudget.setTypeface(myCustomFont1); */

        HomeData = new ArrayList<>();
        BudgetList = new ArrayList<>();

        nombreDePieceChoisi = getIntent().getStringExtra("nombreDePieceChoisi");

        final RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        int resultPiece = nombreDePieceChoisi.compareTo("1");
        if(resultPiece == 0) {
            listingsReference
                    .orderByChild("type")
                    .equalTo("Studio")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                        HomeModel home = child.getValue(HomeModel.class);
                                        home.setHomeID(child.getRef().getKey().toString());
                                        HomeData.add(home);
                                        BudgetList.add(home.getPrix());
                                        Log.v("ERROR", String.valueOf(HomeData.size()));
                                    }
                                    TrieForBudget();
                                    mRecyclerView.invalidate();

                                    adapter.notifyDataSetChanged();
                                }
                            });

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.v("ERROR", databaseError.toString());
                        }
                    });


        }else {

            HomeData = getIntent().getParcelableArrayListExtra("homeData");

            for(HomeModel item : HomeData) {
                int result = nombreDePieceChoisi.compareTo(String.valueOf(item.getPiece()));
                if(result == 0){
                    BudgetList.add(item.getPiece());
                }
            }

            TrieForBudget();
        }





        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        adapter = new BudgetAdapter(this , HomeData,BudgetList,nombreDePieceChoisi);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);



    }



    private void TrieForBudget()
    {
        Collections.sort(BudgetList);

        /*
        *
        * RECUPERATION DES BUDGETS DE MANNIERE IRREPETITIF
         */
        Boolean condition = true;

        while (condition){
            condition = false;
            for(int i = 0;i < BudgetList.size();i++){
                if(i < BudgetList.size()-1){
                    int result = BudgetList.get(i).compareTo(BudgetList.get(i+1));
                    if(result == 0){
                        BudgetList.remove(i);
                        condition = true;
                    }
                }

            }
        }
    }


}

