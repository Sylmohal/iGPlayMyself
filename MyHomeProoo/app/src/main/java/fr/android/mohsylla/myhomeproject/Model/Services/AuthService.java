package fr.android.mohsylla.myhomeproject.Model.Services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by bios on 26/09/2017.
 */

public class AuthService {

    public static FirebaseAuth auth(){
        return FirebaseAuth.getInstance();
    }

    public static String getUserID(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
    public static Boolean CheckIfUserIsConnected(Context context){
        SharedPreferences preferences = context.getSharedPreferences("ID", Context.MODE_PRIVATE);
        String value = preferences.getString("userID",null);
        if(value == null){
            return false;
        }{
            return true;
        }
    }
    public static void Deconnexion(Context context){
        FirebaseAuth.getInstance().signOut();
        SharedPreferences preferences = context.getSharedPreferences("ID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
