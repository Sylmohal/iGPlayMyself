package fr.android.mohsylla.myhomeproject.Activities;


import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;

import fr.android.mohsylla.myhomeproject.Fragment.MeConnectingFragment;
import fr.android.mohsylla.myhomeproject.Fragment.MeFragment;
import fr.android.mohsylla.myhomeproject.R;
import io.fabric.sdk.android.Fabric;

public class DossierOptionnelRegister extends AppCompatActivity {

    Button mValider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dossier_optionnel_register);
      //  getSupportActionBar().hide();
        Fabric.with(this, new Crashlytics());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOptionnelActivity);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mValider = (Button) findViewById(R.id.buttonValider);
        mValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getApplicationContext(), TypeDeBiensActivity.class);
                startActivity(intent);*/

              // final Intent intent;
                Intent intent = new Intent();
                setResult(2,intent);
                finish();

//                FragmentManager manager = getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = manager.beginTransaction();
//                MeConnectingFragment meConnectingFragment = new MeConnectingFragment();
//                fragmentTransaction.replace(R.id.main_container, meConnectingFragment);
//              //  getIntent().putExtra("KEY", 1);
//                fragmentTransaction.commit();

            }
        });


    }

    @Override
    public void onBackPressed() {

    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
////        switch (item.getItemId()){
////            case android.R.id.home:{
////                onBackPressed();
////                return true;
////            }
////            default:{
////                return super.onOptionsItemSelected(item);
////            }
////        }
//    }
}
