package fr.android.mohsylla.myhomeproject.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import fr.android.mohsylla.myhomeproject.R;

public class GalleryFragmentDetails extends AppCompatActivity {

    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_fragment_details);


        getSupportActionBar().setTitle("");
        imageView = (ImageView) findViewById(R.id.detail_image);
        imageView.setImageResource(getIntent().getIntExtra("imageId",00));

    }
}
