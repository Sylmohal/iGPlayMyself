package fr.android.mohsylla.myhomeproject.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import fr.android.mohsylla.myhomeproject.Model.Models.UserModel;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;
import io.fabric.sdk.android.Fabric;

public class RegisterActivity extends AppCompatActivity {



    Button mRegister;
    public FirebaseAuth mAuth;
    private EditText mEmailEditText, mPasswordEditText, mNameEditText,mNumeroEditText,mPrenomTxt;
    SharedPreferences preferences;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Fabric.with(this, new Crashlytics());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarRegisterActivity);
        toolbar.setNavigationIcon(R.mipmap.deleteview_icon);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progression);
        mNameEditText = (EditText) findViewById(R.id.id_name_register);
        mEmailEditText = (EditText) findViewById(R.id.id_email_register);
        mPasswordEditText = (EditText) findViewById(R.id.id_password_register);
        mNumeroEditText = (EditText) findViewById(R.id.id_number_register);
        mPrenomTxt = (EditText) findViewById(R.id.id_prenom_register);

        preferences = getSharedPreferences("ID", Context.MODE_PRIVATE);
        mAuth = FirebaseAuth.getInstance();

        mRegister = (Button) findViewById(R.id.buttonRegister);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleInscription(view);
            }
        });
    }
    public void HandleInscription(final View view){
        if(mNameEditText.getText().length() == 0 || mPrenomTxt.getText().length() == 0 || mEmailEditText.getText().length() == 0 || mNumeroEditText.getText().length() == 0 || mPasswordEditText.getText().length() == 0){
            Snackbar.make(view,"champs vide",Snackbar.LENGTH_LONG)
                    .setAction("Action",null).show();
        }else {
            progressBar.setVisibility(View.VISIBLE);
            mRegister.setText(" ");
            AuthService.auth().createUserWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull final Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                DataBase.userRef().setValue(new UserModel(mNameEditText.getText().toString(),mPrenomTxt.getText().toString(),mEmailEditText.getText().toString(),"vide","vide"))
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> saveTask) {
                                                if(task.isSuccessful()){
                                                    final SharedPreferences.Editor editor = preferences.edit();
                                                    editor.putString("userID",task.getResult().getUser().getUid().toString());
                                                    editor.putString("userName",mPrenomTxt.getText().toString()+" "+mNameEditText.getText().toString());
                                                    editor.putString("userEmail",mEmailEditText.getText().toString());
                                                    editor.commit();
                                                    progressBar.setVisibility(View.GONE);
                                                    Intent intent = new Intent(getApplicationContext(), DossierOptionnelRegister.class);
                                                    startActivityForResult(intent,2);
                                                    mRegister.setText("Valider");
                                                }
                                            }
                                        });
                            }else {
                                progressBar.setVisibility(View.GONE);
                                mRegister.setText("Valider");
                                Snackbar.make(view,task.getException().getLocalizedMessage(),Snackbar.LENGTH_LONG)
                                        .setAction("Action",null).show();
                            }
                        }
                    });

        }

    }


    //startActivity(intent);
          /*     if(mEmailEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Email Vide",Toast.LENGTH_SHORT).show();
                }
                if(mPasswordEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Password Vide",Toast.LENGTH_SHORT).show();
                }
                if(mNameEditText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),"Password Vide",Toast.LENGTH_SHORT).show();
                }

                if(mEmailEditText.getText().length() != 0 && mPasswordEditText.getText().length() != 0){
                    mAuth.createUserWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){

                                        String userID = task.getResult().getUser().getUid().toString();

                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("userID",userID);
                                        editor.commit();
                                        UserModel userModel = new UserModel(mNameEditText.getText().toString(),mEmailEditText.getText().toString(),userID);
                                        myRef.child(userID).setValue(userModel);
                                        Intent intent = new Intent(getApplicationContext(), TypeDeBiensActivity.class);
                                        startActivity(intent);

                                    }else {
                                        Toast.makeText(getApplicationContext(),task.getException().toString(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
            }

            @Override
            protected void onStart() {
                super.onStart();

                String userID = preferences.getString("userID", null);
                if (userID != null) {
                    Intent intent = new Intent(this, TypeDeBiensActivity.class);
                    startActivity(intent);
                }
            }
        }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("code 2",String.valueOf(resultCode));
        if(resultCode == 2){
            Intent intent = new Intent();
            setResult(1,intent);
            finish();
        }
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case android.R.id.home:{
//                onBackPressed();
//                return true;
//            }
//            default:{
//                return super.onOptionsItemSelected(item);
//            }
//        }
//    }

}