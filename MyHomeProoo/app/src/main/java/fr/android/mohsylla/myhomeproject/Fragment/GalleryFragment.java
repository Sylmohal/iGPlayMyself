package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.InfoGalleryAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.ImageModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment {

    RecyclerView recyclerView;
    InfoGalleryAdapter adapter;
    private ArrayList<ImageModel> GalleryList;

    public ArrayList<ImageModel> getGalleryList() {
        return GalleryList;
    }

    public void setGalleryList(ArrayList<ImageModel> galleryList) {
        GalleryList = galleryList;
    }

    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gallery,container,false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview1);
        adapter = new InfoGalleryAdapter(this ,GalleryList);

        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3, GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);

        return v;


    }


}
