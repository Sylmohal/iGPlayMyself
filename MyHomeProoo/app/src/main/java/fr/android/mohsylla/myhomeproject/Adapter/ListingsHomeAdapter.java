package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.ShowHomeDetailsActivity;
import fr.android.mohsylla.myhomeproject.Activities.LoginActivity;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Models.UserWishListModel;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 03/05/2017.
 */



public class ListingsHomeAdapter extends RecyclerView.Adapter<ListingsHomeAdapter.MyViewHolder> {

    Context context;
    ArrayList<HomeModel> data;
    LayoutInflater inflater;

    public ListingsHomeAdapter(Context context , ArrayList<HomeModel> data) {

        this.context= context;
        this.data = data;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.list_item_house_recycler,parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        myViewHolder.SetImage(data.get(position));
        myViewHolder.setBtnTag(position);
        CheckIfHomeIsLiked(position,myViewHolder.likeBtn);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void CheckIfHomeIsLiked(int position, final Button homeBtn){
        if(AuthService.CheckIfUserIsConnected(context)){
            final HomeModel homeModel = data.get(position);
            if(homeModel.getLike()){
                switch (homeModel.getState()){
                    case "visite":
                        homeBtn.setBackgroundResource(R.mipmap.likehome_visite_icon);
                        break;
                    case  "wish":
                        homeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                        break;
                    default:
                        break;
                }
            }
        }

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private Button likeBtn;
        private final Context context;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        View mView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.img_row);
            likeBtn = (Button) itemView.findViewById(R.id.likeButton);

            context = itemView.getContext();
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent; intent =  new Intent(context.getApplicationContext(), ShowHomeDetailsActivity.class);
                    intent.putExtra("homeData",data.get(position));
                    context.startActivity(intent);
                }
            });

            likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
               HandleLikeHome((Button)view,getAdapterPosition());
                }

            });
        }

        public void setBtnTag(int tag) {
            this.likeBtn.setTag(tag);
        }

        public void HandleLikeHome(final Button btn, int position){
            if(AuthService.CheckIfUserIsConnected(context)){
                final HomeModel homeModel = data.get(position);
                if(homeModel.getLike()){
                    switch (homeModel.getState()){
                        case "visite":
                            CancleVisitation(homeModel,btn);
                            break;
                        case "wish":
                            DataBase.userWishList().child(homeModel.getHomeID()).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if(databaseError == null){
                                        btn.setBackgroundResource(R.mipmap.unlike);
                                        homeModel.setLike(false);
                                    }
                                }
                            });
                            break;
                        default:
                            break;
                    }
                } else {
                    homeModel.setLike(true);
                    homeModel.setState("wish");
                    UserWishListModel wishListModel = new UserWishListModel(homeModel.getType(),"wish",homeModel.getPossessionDuBien());
                    DataBase.userWishList().child(homeModel.getHomeID()).setValue(wishListModel);
                    btn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                }
            }else {
                Intent intent; intent =  new Intent(context.getApplicationContext(), LoginActivity.class);
                context.startActivity(intent);
            }
        }
        public void CancleVisitation(final HomeModel home,final Button btn){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Visitation");
            builder.setMessage("Voulez vous annuler la visite ?");
            builder.setCancelable(false);

            builder.setPositiveButton("non", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.setNegativeButton("oui", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    DataBase.visiteRef().child(home.getHomeID()).removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError == null){
                                home.setState("wish");
                                btn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                                DataBase.userWishList().child(home.getHomeID()).child("state").setValue("wish");
                            }
                        }
                    });
                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        public void SetImage(HomeModel item){
            StorageReference storageReference = storage.getReference(item.getImagePrincipale().getPath());
            Glide.with(context.getApplicationContext())
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(imageView);
        }

    }
}
