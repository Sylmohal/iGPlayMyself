package fr.android.mohsylla.myhomeproject.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import fr.android.mohsylla.myhomeproject.Activities.DossierOptionnelRegister;
import fr.android.mohsylla.myhomeproject.utils.SimpleDividerItemDecoration;
import fr.android.mohsylla.myhomeproject.Adapter.ProfileAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataProfileDeconnexion;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeConnectedFragment extends Fragment {
    RecyclerView recyclerView;
    ProfileAdapter adapter;
    Button mFinaliser;


    public MeConnectedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_me_connected,container,false);


        // SHOW USER INFORMATIONS

        SharedPreferences preferences = getActivity().getSharedPreferences("ID", Context.MODE_PRIVATE);
        String nameValue = preferences.getString("userName",null);
        String emailValue = preferences.getString("userEmail",null);

        ((TextView)v.findViewById(R.id.nom_user)).setText(nameValue);
        ((TextView)v.findViewById(R.id.email_user)).setText(emailValue);


        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerProfile);
        adapter = new ProfileAdapter(getActivity(), DataProfileDeconnexion.getData());

        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        mFinaliser = (Button) v.findViewById(R.id.buttonFinaliser);
        mFinaliser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DossierOptionnelRegister.class);
                startActivity(intent);
            }
        });

        ProgressBar simpleProgressBar=(ProgressBar) v.findViewById(R.id.simpleProgressBar);
        simpleProgressBar.setMax(100);
        simpleProgressBar.setProgress(50);
        simpleProgressBar.setScaleY(3f);
        simpleProgressBar.setBackground(getActivity().getDrawable(R.drawable.input_norm));

        return v;
    }


}
