package fr.android.mohsylla.myhomeproject.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import fr.android.mohsylla.myhomeproject.Activities.RechercheMultiCriteresActivity;
import fr.android.mohsylla.myhomeproject.Adapter.TabPagerParallaxAdapter;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    NombredePiecesFragment mNombredePiecesFragment;
    TabPagerParallaxAdapter mAdapter;

    public MainFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_main, container, false);


        Log.v("TAB", "OnCreateView");

       Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbarMyHome);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);


        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        mTabLayout = (TabLayout) v.findViewById(R.id.tab_layout_searchFragment);
        mViewPager = (ViewPager) v.findViewById(R.id.view_pager_search_fragment);

        mAdapter = new TabPagerParallaxAdapter(getChildFragmentManager());

        mTabLayout.setupWithViewPager(mViewPager);

        SearchFragment searchFragment = new SearchFragment();
        mAdapter.addFragment(searchFragment,"Location");


        SearchFragmentAchat searchFragmentAchat = new SearchFragmentAchat();
        mAdapter.addFragment(searchFragmentAchat,"Achat");


        mViewPager.setAdapter(mAdapter);

        return v;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("TAB", "OnCreate");
        setHasOptionsMenu(true);
        setRetainInstance(true);

    }


    @Override
    public void onResume() {
        super.onResume();

    }

      @Override
    public void onStart() {
        super.onStart();

    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
        Log.v("TAB", "OnCreateOptionsMenu");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        if (id == R.id.filter_search_type) {

            Intent intent = new Intent(getContext(), RechercheMultiCriteresActivity.class);
            startActivity(intent);


        }

        return super.onOptionsItemSelected(item);
    }



}
