package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Fragment.BudgetFragment;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 15/05/2017.
 */

public class AppartAdapter extends RecyclerView.Adapter<AppartAdapter.MyViewHolder> {

    Context context;
    ArrayList<String> data;
    LayoutInflater inflater;
    ArrayList<HomeModel> HomeData;
    Bundle bundle = new Bundle();


    public AppartAdapter(Context context, ArrayList<String> data, ArrayList<HomeModel> HomeData) {

        this.context = context;
        this.data = data;
        this.HomeData = HomeData;
        inflater = LayoutInflater.from(context);

    }


    @Override
    public AppartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_appart, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final AppartAdapter.MyViewHolder holder, final int position) {
        holder.tv_item_name.setText(String.valueOf(data.get(position)));
       holder.mView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               bundle.putString("nombreDePieceChoisi",data.get(position));
               bundle.putParcelableArrayList("homeData",HomeData);
               

               FragmentManager manager = ((AppCompatActivity)holder.context).getSupportFragmentManager();
               FragmentTransaction fragmentTransaction = manager.beginTransaction();
               BudgetFragment fragmentBudget = new BudgetFragment();
               fragmentTransaction.replace(R.id.main_container, fragmentBudget);
               fragmentTransaction.addToBackStack(null);
               fragmentTransaction.commit();

               fragmentBudget.setArguments(bundle);
             /*  Intent intent = new Intent(holder.context,BudgetActivity.class);
               intent.putExtra("nombreDePieceChoisi",String.valueOf(data.get(position)));
               intent.putParcelableArrayListExtra("homeData",HomeData); */
             //  holder.context.startActivity(intent);

           }
       });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_item_name;
        View mView;
        final Context context;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_name);
            mView = itemView;
            context = itemView.getContext();
        }
    }

}
