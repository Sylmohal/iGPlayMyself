package fr.android.mohsylla.myhomeproject.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import fr.android.mohsylla.myhomeproject.Adapter.SliderAdapter;
import fr.android.mohsylla.myhomeproject.Adapter.TabPagerParallaxAdapter;
import fr.android.mohsylla.myhomeproject.Fragment.GalleryFragment;
import fr.android.mohsylla.myhomeproject.Fragment.InfoAppartFragment;
import fr.android.mohsylla.myhomeproject.Fragment.InfoStudioFragment;
import fr.android.mohsylla.myhomeproject.Fragment.InfoVillaFragment;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Models.ImageModel;
import fr.android.mohsylla.myhomeproject.Model.Models.UserWishListModel;
import fr.android.mohsylla.myhomeproject.Model.Models.VisiteHomeModel;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;
import io.fabric.sdk.android.Fabric;

public class ShowHomeDetailsActivity extends AppCompatActivity {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    ViewPager viewPagerSlide;
    SliderAdapter adapter;
    Handler handler;
    Runnable runnable;
    Timer timer;
    private HomeModel homeToShowDetails;
    private ImageView imgHeader;
    private Button likeHomeBtn;
    private Button visiteBtn;


    FirebaseStorage storage = FirebaseStorage.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_show_home_details);
        SetUpHeaderAndNavigation();

//        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
//        int result = api.isGooglePlayServicesAvailable(this);
//        if(result == ConnectionResult.SUCCESS){
//            Toast.makeText(this,"good",Toast.LENGTH_SHORT).show();
//        }else if (api.isUserResolvableError(result)){
//            Dialog dialog = api.getErrorDialog(this,result,0);
//            dialog.show();
//        }else {
//            Toast.makeText(this,"Cant connect to play services",Toast.LENGTH_SHORT).show();
//        }

       // View view = findViewById(R.id.detailsHouse);

     /* Snackbar snackbar = Snackbar.make(view,"SnackBar",Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(R.color.colorMyhome);
        snackbar.show(); */
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void SetUpHeaderAndNavigation() {


         homeToShowDetails = getIntent().getParcelableExtra("homeData");

        //mettre l'image principale dans la gallerie
        ArrayList<ImageModel> imageList = new ArrayList<>();
        imageList.add(homeToShowDetails.getImagePrincipale());
        for (ImageModel img : homeToShowDetails.getImageGallerie()) {
            imageList.add(img);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        likeHomeBtn = (Button)findViewById(R.id.likeButton);
        visiteBtn = (Button)findViewById(R.id.visiteBtn);


        if(homeToShowDetails.getLike()){
            switch (homeToShowDetails.getState()){
                case "wish":
                    likeHomeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                    break;
                case "visite":
                    likeHomeBtn.setBackgroundResource(R.mipmap.likehome_visite_icon);
                    visiteBtn.setText("j\'annule");
                    visiteBtn.setBackgroundTintList(this.getResources().getColorStateList(R.color.colorGray));
                    break;
                default:break;
            }
        }else {
            likeHomeBtn.setBackgroundResource(R.mipmap.unlike);
        }


        likeHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleLikeHome();
            }
        });

        visiteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleHomeVisite();
            }
        });


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        viewPagerSlide = (ViewPager) findViewById(R.id.view_pager2);
        adapter = new SliderAdapter(this,imageList);
        viewPagerSlide.setAdapter(adapter);


        handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    int i = viewPagerSlide.getCurrentItem();


                    if (i == adapter.images.size()-1){
                        i=0;
                        viewPagerSlide.setCurrentItem(i);
                    } else {

                        i++;
                        viewPagerSlide.setCurrentItem(i);
                    }

                }
            };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        },4000 , 4000);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        imgHeader = (ImageView) findViewById(R.id.img_header);




        TabPagerParallaxAdapter mAdapter = new TabPagerParallaxAdapter(getSupportFragmentManager());

        switch (homeToShowDetails.getType()){
            case "Studio":
                InfoStudioFragment infoStudioFragment = new InfoStudioFragment();
                infoStudioFragment.setHomedata(homeToShowDetails);
                mAdapter.addFragment(infoStudioFragment,"Infos");
                break;
            case "Villa":
                InfoVillaFragment infoVillaFragment = new InfoVillaFragment();
                infoVillaFragment.setHomedata(homeToShowDetails);
                mAdapter.addFragment(infoVillaFragment,"Infos");
                break;
            case "Appartement":
                InfoAppartFragment  infoAppartFragment = new InfoAppartFragment();
                infoAppartFragment.setHomedata(homeToShowDetails);
                mAdapter.addFragment(infoAppartFragment, "Infos");
                break;
            default:break;

        }
      /*  PaymentFragment paymentFragment = new PaymentFragment();
        paymentFragment.setHomeData(homeModel);
        mAdapter.addFragment(paymentFragment , "Payment"); */

        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.setGalleryList(imageList);

        mAdapter.addFragment(galleryFragment, "Gallerie");

            mViewPager.setAdapter(mAdapter);
            mTabLayout.setupWithViewPager(mViewPager);

        // save tabLayoutPosition

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        TextView prixTV = (TextView)findViewById(R.id.nested_price);
        prixTV.setText(homeToShowDetails.getPrix()+" FCFA");

        TextView localityTV = (TextView)findViewById(R.id.nested_location);
        localityTV.setText(homeToShowDetails.getCommune()+" - "+ homeToShowDetails.getSecteur());

        TextView pieceAndtypeTV = (TextView)findViewById(R.id.nested_piece);
        switch (homeToShowDetails.getPossessionDuBien()){
            case "Location":
                int result = homeToShowDetails.getType().compareTo("Studio");
                if(result == 0) {
                    pieceAndtypeTV.setText("Studio à louer");
                }else  {
                    pieceAndtypeTV.setText(homeToShowDetails.getType()+" "+ homeToShowDetails.getPiece()+" pieces à louer");
                }
                break;
            case "Vente":
                pieceAndtypeTV.setText(homeToShowDetails.getType()+" "+ homeToShowDetails.getPiece()+" pieces à vendre");
                break;
            default:
                break;
        }

    }

    public void HandleHomeVisite(){
        if(AuthService.CheckIfUserIsConnected(this)){
            if(homeToShowDetails.getLike()){
                switch (homeToShowDetails.getState()){
                    case "wish":
                        DataBase.visiteRef().child(homeToShowDetails.getHomeID()).setValue(new VisiteHomeModel("visitation"));
                        DataBase.userWishList().child(homeToShowDetails.getHomeID()).child("state").setValue("visite", new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if(databaseError == null){
                                    homeToShowDetails.setState("visite");
                                    visiteBtn.setText("j\'annule");
                                    likeHomeBtn.setBackgroundResource(R.mipmap.likehome_visite_icon);
                                    visiteBtn.setBackgroundTintList(ShowHomeDetailsActivity.this.getResources().getColorStateList(R.color.colorGray));
                                }
                            }
                        });
                        visiteConfirmation();
                        break;
                    case "visite":
                          CancleVisitation();
                        break;
                    default:break;

                }
            }else {

                DataBase.visiteRef().child(homeToShowDetails.getHomeID()).setValue(new VisiteHomeModel("visitation"));
                DataBase.userWishList().child(homeToShowDetails.getHomeID()).setValue(new UserWishListModel(homeToShowDetails.getType(),"visite",homeToShowDetails.getPossessionDuBien()), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if(databaseError == null){
                            homeToShowDetails.setLike(true);
                            homeToShowDetails.setState("visite");
                            visiteBtn.setText("j\'annule");
                            likeHomeBtn.setBackgroundResource(R.mipmap.likehome_visite_icon);
                            visiteBtn.setBackgroundTintList(ShowHomeDetailsActivity.this.getResources().getColorStateList(R.color.colorGray));
                        }
                    }
                });
                visiteConfirmation();
            }
        } else {
            Intent intent; intent =  new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }
    }
    public void CancleVisitation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowHomeDetailsActivity.this);
        builder.setTitle("Visitation");
        builder.setMessage("Voulez vous annuler la visite ?");
        builder.setCancelable(false);

        builder.setPositiveButton("non", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNegativeButton("oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DataBase.visiteRef().child(homeToShowDetails.getHomeID()).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if(databaseError == null){
                            homeToShowDetails.setState("wish");
                            visiteBtn.setText("je visite");
                            visiteBtn.setBackgroundTintList(ShowHomeDetailsActivity.this.getResources().getColorStateList(R.color.colorMyhome));
                            likeHomeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                            DataBase.userWishList().child(homeToShowDetails.getHomeID()).child("state").setValue("wish");
                        }
                    }
                });
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    public void visiteConfirmation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowHomeDetailsActivity.this);
        builder.setTitle("Visitation");
        builder.setMessage("Voulez vous continuer la recherche ou passer aux visites ?");
        builder.setCancelable(false);

        builder.setPositiveButton("continuer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNegativeButton("visiter", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DataBase.notificationRef().setValue(new VisiteHomeModel("ready"));
            }
        });

        final AlertDialog alertDialog = builder.create();

        DataBase.notificationRef().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    alertDialog.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void HandleLikeHome(){
        if(AuthService.CheckIfUserIsConnected(this)){
            if(homeToShowDetails.getLike()){
                switch (homeToShowDetails.getState()){
                    case "visite":
                      CancleVisitation();
                        break;
                    case "wish":
                        DataBase.userWishList().child(homeToShowDetails.getHomeID()).removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if(databaseError == null){
                                    likeHomeBtn.setBackgroundResource(R.mipmap.unlike);
                                    homeToShowDetails.setLike(false);
                                }
                            }
                        });

                        break;
                    default:
                        break;
                }
            } else {
                homeToShowDetails.setLike(true);
                homeToShowDetails.setState("wish");
                UserWishListModel wishListModel = new UserWishListModel(homeToShowDetails.getType(),"wish",homeToShowDetails.getPossessionDuBien());
                DataBase.userWishList().child(homeToShowDetails.getHomeID()).setValue(wishListModel);
                likeHomeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
            }
        }else {
            Intent intent; intent =  new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }
    }

}
