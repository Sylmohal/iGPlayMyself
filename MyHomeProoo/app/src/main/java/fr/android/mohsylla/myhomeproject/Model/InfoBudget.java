package fr.android.mohsylla.myhomeproject.Model;

/**
 * Created by bios on 20/07/2017.
 */

public class InfoBudget {

    private String budget;

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }
}
