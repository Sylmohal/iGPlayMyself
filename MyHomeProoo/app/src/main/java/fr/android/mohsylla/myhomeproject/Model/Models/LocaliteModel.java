package fr.android.mohsylla.myhomeproject.Model.Models;

import java.util.ArrayList;

/**
 * Created by bios on 27/09/2017.
 */

public class LocaliteModel {
    private String Commune;
    private ArrayList<String> secteur ;

    public String getCommune() {
        return Commune;
    }

    public void setCommune(String commune) {
        Commune = commune;
    }

    public ArrayList<String> getSecteur() {
        return secteur;
    }

    public void setSecteur(ArrayList<String> secteur) {
        this.secteur = secteur;
    }

    public LocaliteModel(String commune, ArrayList<String> secteur) {
        Commune = commune;
        this.secteur = secteur;
    }
}
