package fr.android.mohsylla.myhomeproject.Model.Models;

/**
 * Created by bios on 03/10/2017.
 */

public class VisiteHomeModel {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public VisiteHomeModel() {
    }

    public VisiteHomeModel(String state) {
        this.state = state;
    }
}
