package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Model.Info;
import fr.android.mohsylla.myhomeproject.Model.InfoAppart;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by Mohsylla on 04/10/2017.
 */

public class InfoAppartAdapter extends RecyclerView.Adapter<InfoAppartAdapter.InfoAppartViewHolder> {

    private Context mContext;
    private HomeModel homeData;


    public InfoAppartAdapter(Context context, HomeModel home) {
        mContext = context;
        this.homeData = home;
    }

    @Override
    public InfoAppartAdapter.InfoAppartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InfoAppartViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_info_parallax, parent, false));
    }

    @Override
    public void onBindViewHolder(InfoAppartAdapter.InfoAppartViewHolder holder, int position) {

        switch (position) {
            case 0:
                holder.tvCount.setText("" + homeData.getChambre());
                holder.tvTitle.setText("CHAMBRE");
                holder.imgIcon.setImageResource(R.mipmap.chambre);
                break;
            case 1:
                holder.tvTitle.setText("DOUCHE");
                holder.tvCount.setText("" + homeData.getDouche());
                holder.imgIcon.setImageResource(R.mipmap.douche);
                break;
        }
    }

        @Override
        public int getItemCount () {
            return 2;
        }

        public class InfoAppartViewHolder extends RecyclerView.ViewHolder {

            ImageView imgIcon;
            TextView tvTitle, tvCount;

            public InfoAppartViewHolder(View itemView) {

                super(itemView);

                imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvCount = (TextView) itemView.findViewById(R.id.tv_count);
            }
        }
    }



