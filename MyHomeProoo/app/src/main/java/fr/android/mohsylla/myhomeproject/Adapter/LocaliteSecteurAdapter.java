package fr.android.mohsylla.myhomeproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Activities.ListingsHomeActivity;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.R;

/**
 * Created by bios on 28/09/2017.
 */

public class LocaliteSecteurAdapter extends RecyclerView.Adapter<LocaliteSecteurAdapter.SecteurViewHolder> {

    ArrayList<String> secteurList;
    ArrayList<HomeModel> HomeData;
    LayoutInflater inflater;

    int selectedPosition = -1;

    public LocaliteSecteurAdapter(Context context, ArrayList<String> secteurList, ArrayList<HomeModel> homeData) {
        inflater = LayoutInflater.from(context);
        this.secteurList = secteurList;
        HomeData = homeData;
    }

    @Override
    public SecteurViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_localite_secteur,parent,false);
        return new SecteurViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SecteurViewHolder holder, int position) {
        holder.tv_item_location.setText(secteurList.get(position));
        holder.radioButton.setChecked(position == selectedPosition);
        holder.tv_item_location.setTag(position);
        holder.radioButton.setTag(position);
    }

    @Override
    public int getItemCount() {
        return secteurList.size();
    }
    private void itemCheckChanged(View v) {
        selectedPosition = (int)v.getTag();
        notifyDataSetChanged();
    }

    public class SecteurViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_item_location;
        RadioButton radioButton;
        Context context;
        View viewBottomBar;
        public SecteurViewHolder(View itemView) {
            super(itemView);
            tv_item_location = (TextView) itemView.findViewById(R.id.tv_item_budget);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio_button);
            viewBottomBar = (View) itemView.findViewById(R.id.viewBottomBar);

            context = itemView.getContext();
            tv_item_location.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {

            final Intent intent;
            int position = getAdapterPosition();
            itemCheckChanged(view);

            intent = new Intent(context, ListingsHomeActivity.class);
            intent.putParcelableArrayListExtra("homeData",HomeData);
            intent.putExtra("localiteChoisi",secteurList.get(position));
            context.startActivity(intent);
        }
    }
}