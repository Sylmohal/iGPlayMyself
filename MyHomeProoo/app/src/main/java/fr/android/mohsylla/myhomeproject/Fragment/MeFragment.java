package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.android.mohsylla.myhomeproject.Adapter.ProfileAdapter;
import fr.android.mohsylla.myhomeproject.utils.SimpleDividerItemDecoration;
import fr.android.mohsylla.myhomeproject.Datas.DataProfile;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeFragment extends Fragment {
    RecyclerView recyclerView;
    ProfileAdapter adapter;


    public MeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_me,container,false);



     //   ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerProfile);
            adapter = new ProfileAdapter(getActivity(),DataProfile.getData());
        //   imageView = (ImageView) v.findViewById(R.id.img_row);


       /* ProgressBar simpleProgressBar=(ProgressBar) v.findViewById(R.id.simpleProgressBar);
        simpleProgressBar.setMax(100);
        simpleProgressBar.setProgress(50);
        simpleProgressBar.setScaleY(3f); */

        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        return v;

    }

}
