package fr.android.mohsylla.myhomeproject.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import fr.android.mohsylla.myhomeproject.Activities.MainActivity;
import fr.android.mohsylla.myhomeproject.Activities.TypeDeBiensActivity;
import fr.android.mohsylla.myhomeproject.Adapter.FavorisAdapter;
import fr.android.mohsylla.myhomeproject.Datas.DataFavoris;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Models.UserWishListModel;
import fr.android.mohsylla.myhomeproject.Model.Services.AuthService;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;

import static fr.android.mohsylla.myhomeproject.R.id.bottomNavigationView;
import static fr.android.mohsylla.myhomeproject.R.id.likeButton;

/**
 * A simple {@link Fragment} subclass.
 *
 *
 * **/



public class FavorisFragment extends Fragment {


    public ProgressBar progressBar;
    Button mAddMaison;
    RecyclerView recyclerViewFavoris;
    FavorisAdapter favorisAdapter;
    String btnState;

    public FavorisFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_favoris, container, false);

        //    ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
         recyclerViewFavoris = (RecyclerView) view.findViewById(R.id.recyclerviewFavoris);
         mAddMaison = (Button) view.findViewById(R.id.buttonAddFavoris);
         progressBar = (ProgressBar) view.findViewById(R.id.homeProgress);

        if(AuthService.CheckIfUserIsConnected(getActivity())){
            new FavorisQueryTask().execute();
        }else {
            ChangeBtnAttribute("wish");
        }


        final BottomNavigationView bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.bottomNavigationView);


        mAddMaison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainFragment mainFragment = new MainFragment();
                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.main_container, mainFragment);
                // fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                bottomNavigationView.setSelectedItemId(R.id.id_recherche);

            }
        });


        return view;
    }

    public class FavorisQueryTask extends AsyncTask<Void,Void,ArrayList<HomeModel>> {

        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {

            final ArrayList<HomeModel> list = new ArrayList<>();
            progressBar.setVisibility(View.VISIBLE);

            DataBase.userWishList().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot wishDataSnapshot) {
                    if(wishDataSnapshot.exists()){
                        list.clear();
                        for(DataSnapshot wishItem: wishDataSnapshot.getChildren()){
                            final UserWishListModel wish = wishItem.getValue(UserWishListModel.class);
                            DataBase.listingsHomeRef()
                                    .child(wish.getPossessionDuBien())
                                    .child(wish.getTypeDuBien())
                                    .child(wishItem.getKey())
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot homeDataSnapshot) {
                                            HomeModel homeModel = homeDataSnapshot.getValue(HomeModel.class);
                                            homeModel.setLike(true);
                                            homeModel.setState(wish.getState());
                                            homeModel.setType(wish.getTypeDuBien());
                                            homeModel.setPossessionDuBien(wish.getPossessionDuBien());
                                            homeModel.setHomeID(homeDataSnapshot.getKey());
                                            list.add(homeModel);

                                            Collections.sort(list, new Comparator<HomeModel>() {
                                                @Override
                                                public int compare(HomeModel homeModel, HomeModel t1) {
                                                    return homeModel.getState().compareTo("wish");
                                                }
                                            });

                                            int result = list.get(0).getState().compareTo("visite");
                                            if(result == 0){
                                                ChangeBtnAttribute("visite");
                                            }else {
                                                ChangeBtnAttribute("wish");
                                            }

                                            progressBar.setVisibility(View.GONE);
                                            recyclerViewFavoris.invalidate();
                                            favorisAdapter.notifyDataSetChanged();

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                        /*
                        * VERIFIER SI LA DEMANDE DE VISITE EST OK
                         */
                        DataBase.notificationRef().addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    ChangeBtnAttribute("demandeIsDone");
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } else {
                        ChangeBtnAttribute("wish");
                        progressBar.setVisibility(View.GONE);
                        list.clear();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> homeList) {
            favorisAdapter = new FavorisAdapter(FavorisFragment.this , homeList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
            recyclerViewFavoris.setLayoutManager(linearLayoutManager);
            recyclerViewFavoris.setAdapter(favorisAdapter);
        }
    }

    private void ChangeBtnAttribute(String state){
        mAddMaison.setVisibility(View.VISIBLE);
        switch (state){
            case "visite":
                btnState = "visite";
                mAddMaison.setTextColor(getActivity().getResources().getColor(R.color.white));
                mAddMaison.setBackgroundTintList(getResources().getColorStateList(R.color.colorMyhome));
                mAddMaison.setBackground(getActivity().getDrawable(R.drawable.input_norm));
                mAddMaison.setText("Demander une visite");
                break;
            case "wish":
                btnState = "wish";
                mAddMaison.setTextColor(getActivity().getResources().getColor(R.color.colorWish));
                mAddMaison.setBackgroundTintList(null);
                mAddMaison.setBackground(getActivity().getDrawable(R.drawable.input_out2));
                mAddMaison.setText("Ajouter une maison");
                break;
            case "demandeIsDone":
                btnState = "demandeIsDone";
                mAddMaison.setEnabled(false);
                mAddMaison.setTextColor(getActivity().getResources().getColor(R.color.white));
                mAddMaison.setBackgroundTintList(getResources().getColorStateList(R.color.colorGray));
                mAddMaison.setBackground(getActivity().getDrawable(R.drawable.input_norm));
                mAddMaison.setText("Demande en attente");
                break;
            default:break;
        }
    }



  /*  @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }
 */

}
