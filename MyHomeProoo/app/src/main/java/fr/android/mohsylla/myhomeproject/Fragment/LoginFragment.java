package fr.android.mohsylla.myhomeproject.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import fr.android.mohsylla.myhomeproject.Activities.RegisterActivity;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    Button mSeConnnecter , mEnregister;
    public FirebaseAuth mAuth;
    private EditText mEmailEditText,mPasswordEditText;
    SharedPreferences preferences;
    TextView mDisplaypassword;
    EditText editText;



    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);

editText = (EditText) v.findViewById(R.id.id_password);
        mDisplaypassword = (TextView) v.findViewById(R.id.afficher_password);
        mDisplaypassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN:
                        editText.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case MotionEvent.ACTION_UP:
                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                }
                return true;
            }

        });

  //      ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        preferences = getActivity().getSharedPreferences("ID", Context.MODE_PRIVATE);

        mSeConnnecter = (Button) v.findViewById(R.id.buttonSeConnecter);
        mEnregister = (Button) v.findViewById(R.id.buttonEnregistrer);
        mEmailEditText = (EditText) v.findViewById(R.id.id_email);
        mPasswordEditText = (EditText) v.findViewById(R.id.id_password);

        mAuth = FirebaseAuth.getInstance();

        mSeConnnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                MeConnectedFragment fragmentMeConnected = new MeConnectedFragment();
//                fragmentTransaction.replace(R.id.main_container, fragmentMeConnected);
//                fragmentTransaction.commit();

//
              
//                if(mEmailEditText.getText().length() == 0){
//                    Toast.makeText(getContext(),"Email Vide",Toast.LENGTH_SHORT).show();
//                }
//                if(mPasswordEditText.getText().length() == 0){
//                    Toast.makeText(getContext(),"Password Vide",Toast.LENGTH_SHORT).show();
//                }
//
//                if(mEmailEditText.getText().length() != 0 && mPasswordEditText.getText().length() != 0){
//                    mAuth.signInWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
//                            .addOnCompleteListener((Executor) LoginFragment.this, new OnCompleteListener<AuthResult>() {
//                                @Override
//                                public void onComplete(@NonNull Task<AuthResult> task) {
//                                    if(task.isSuccessful()){
//
//                                        SharedPreferences.Editor editor = preferences.edit();
//                                        editor.putString("userID",task.getResult().getUser().getUid().toString());
//                                        editor.commit();
//                                        Intent intent = new Intent(getContext(), SearchFragment.class);
//                                        startActivity(intent);
//
//                                    }else {
//                                        Toast.makeText(getContext(),task.getException().toString(),Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                }


            }
        });
        mEnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                RegisterFRagment fragmentRegister = new RegisterFRagment();
                fragmentTransaction.replace(R.id.main_container, fragmentRegister);
                fragmentTransaction.commit(); */

               Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivity(intent);

            }
        });


        return v;
    }




}