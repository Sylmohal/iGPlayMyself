package fr.android.mohsylla.myhomeproject.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fr.android.mohsylla.myhomeproject.Adapter.InfoPaymentAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.InfoPayment;
import fr.android.mohsylla.myhomeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment {


    private View rootView;
    private RecyclerView mRecyclerView;
    private HomeModel HomeData;

    public HomeModel getHomeData() {
        return HomeData;
    }

    public void setHomeData(HomeModel homeData) {
        HomeData = homeData;
    }

    private ArrayList<InfoPayment> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_payment, null);
            initialize();
        }

        return rootView;
    }

    private void initialize() {

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        setDummyData();


        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        InfoPaymentAdapter mAdapter = new InfoPaymentAdapter(getActivity(), data,HomeData);

        mRecyclerView.setAdapter(mAdapter);
    }

    private void setDummyData(){

        InfoPayment infoPayment;

        infoPayment = new InfoPayment();
        infoPayment.setTitle("Loyer");
        infoPayment.setPrice("2800000 Fcfa");
        data.add(infoPayment);

        infoPayment = new InfoPayment();
        infoPayment.setTitle("Caution");
        infoPayment.setPrice("2");
        data.add(infoPayment);

        infoPayment = new InfoPayment();
        infoPayment.setTitle("Mois d'avance");
        infoPayment.setPrice("2");
        data.add(infoPayment);

    }

}
