package fr.android.mohsylla.myhomeproject.Activities;

import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import fr.android.mohsylla.myhomeproject.Adapter.AppartAdapter;
import fr.android.mohsylla.myhomeproject.Model.Models.HomeModel;
import fr.android.mohsylla.myhomeproject.Model.Services.DataBase;
import fr.android.mohsylla.myhomeproject.R;

public class NombreDePieceActivity extends AppCompatActivity {
TextView mNOMBREPIECES , mCHOISISSEZPIECES;
    AppartAdapter adapter;
    private int selectedRoom;
    private String typeDeBienChoisi;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;
    private ArrayList<HomeModel> HomeData ;
    private ArrayList<String> NomDePieceList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appartement);

        mNOMBREPIECES = (TextView) findViewById(R.id.titlePIECES);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Heavy.ttf");
        mNOMBREPIECES.setTypeface(myCustomFont);

      mCHOISISSEZPIECES = (TextView) findViewById(R.id.choisirNOMBREPIECES);
        Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(),"fonts/Avenir-Book.ttf");
        mCHOISISSEZPIECES.setTypeface(myCustomFont1);

        HomeData = new ArrayList<HomeModel>();
        NomDePieceList = new ArrayList<>();

        typeDeBienChoisi = getIntent().getStringExtra("typeDeBien");
        Log.v("type",typeDeBienChoisi);

        // drawer layout

        getSupportActionBar().setTitle("");
     //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
     //   getSupportActionBar().setHomeButtonEnabled(true);
//        drawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout1);
       // actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.drawer_open,R.string.drawer_close);
    //    drawerLayout.setDrawerListener(actionBarDrawerToggle);


      //  AmbergurView ambergurView = new AmbergurView(this);
      //  ambergurView.SetUpNavigation();



        final RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);



        DataBase.listingsHomeRef()
                .orderByChild("type")
                .equalTo(typeDeBienChoisi)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
                        for(DataSnapshot child : dataSnapshot.getChildren()){
                            HomeModel home = child.getValue(HomeModel.class);
                            home.setHomeID(child.getRef().getKey().toString());
                            HomeData.add(home);
                            NomDePieceList.add(home.getPiece());

                        }


                        /*
                        * TRIE DANS L'ORDRE DECROISSANT DU NOMBRE DE PIECE
                         */
                        Collections.sort(NomDePieceList);

                        /*
                        * RECUPERATION DU NOMBRE DE PIECE IRREPETITIF
                         */
                        Boolean conditionDelete = true;

                        while (conditionDelete){
                            conditionDelete = false;
                            for(int i = 0;i < NomDePieceList.size();i++){
                                if(i < NomDePieceList.size()-1){
                                    if(NomDePieceList.get(i) == NomDePieceList.get(i+1)){
                                        NomDePieceList.remove(i);
                                        conditionDelete = true;
                                    }
                                }

                            }
                        }
                        mRecyclerView.invalidate();

                        adapter.notifyDataSetChanged();
//                    }
//                });


            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.v("ERROR",databaseError.toString());
            }
        });

//        DataBase.listingsHomeRef().removeEventListener();

        adapter = new AppartAdapter(this,NomDePieceList,HomeData);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        //set the adapter
        mRecyclerView.setAdapter(adapter);
    }
/*
    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

}
